#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class USDKResourcesImageResource, USDKUserDecision, USDKUserOptions, USDKCONSENT_TYPE, USDKKotlinThrowable, USDKCategory, USDKService, USDKSettings, USDKUsercentricsINITIAL_VIEW, USDKEvent, USDKKotlinEnum, USDKConsentStatusObject, USDKKtor_httpHeadersBuilder, USDKOptionalOptions, USDKApi, USDKDeviceStorage, USDKWarningMessages, USDKKotlinArray, USDKCountryData, USDKLocationData, USDKUserCountry, USDKDataDistribution, USDKLanguage_, USDKProcessingCompany, USDKURLs, USDKBaseService, USDKButton, USDKButtons, USDKCONSENT_STATUS, USDKConsentHistory, USDKConsent, USDKDATA_EXCHANGE_TYPE, USDKDataDistributionTitle, USDKDataExchangeSetting, USDKDescriptionTitle, USDKUISettings, USDKExtendedSettings, USDKFirstLayerDescription, USDKFirstLayer, USDKGeneralLabels, USDKLEGACY_BACKGROUND_OVERLAY_TARGET, USDKLEGACY_DATA_EXCHANGE_TYPE, USDKLEGACY_STRING_TO_LIST_PROPERTIES, USDKLEGACY_VERSION, USDKServiceLabels, USDKLabels, USDKLegacyBackgroundOverlay, USDKLegacyBaseService, USDKLegacyCategory, USDKLegacyDataExchangeSetting, USDKLegacyHashService, USDKLegacyLabels, USDKLegacyService, USDKLegacySettings, USDKLink, USDKLinks, USDKPoweredBy, USDKTabs, USDKSecondLayer, USDKServiceHashArrayObject, USDKURLsTitle, USDKLocation, USDKKotlinx_serialization_runtimeJsonElement, USDKKotlinx_serialization_runtimeJsonTransformingSerializer, USDKVERSION, USDKStorage, USDKStorageSettings, USDKStorageService, USDKStorageKeys, USDKEventManager, USDKDataTransferObject, USDKUserConsentResponse, USDKConsentData, USDKConsentsList, USDKGetConsentsVariables, USDKGraphQLConsent, USDKGraphQLQuery, USDKSaveConsentsVariables, USDKGraphQLQueryMutation, USDKLanguageData, USDKSaveConsents, USDKServicesRequest, USDKUserConsentsList, USDKCONSENT_ACTION, USDKConsent_, USDKVersions, USDKSettingsService, USDKEventDispatcher, USDKMergedServicesSettings, USDKDataTransferObjectConsent, USDKDataTransferObjectSettings, USDKDataTransferObjectService, USDKEssentialServices, USDKKotlinx_coroutines_coreCoroutineDispatcher, UIView, UIColor, NSBundle, UIImage, USDKKtor_utilsStringValuesBuilder, USDKKotlinx_serialization_runtimeSerialKind, USDKKotlinNothing, USDKKotlinx_serialization_runtimeUpdateMode, USDKKotlinx_serialization_runtimeJsonNull, USDKKotlinx_serialization_runtimeJsonPrimitive, USDKKotlinAbstractCoroutineContextElement;

@protocol USDKKotlinComparable, USDKKotlinx_serialization_runtimeKSerializer, USDKLegacyBaseServiceInterface, USDKKotlinx_serialization_runtimeEncoder, USDKKotlinx_serialization_runtimeSerialDescriptor, USDKKotlinx_serialization_runtimeSerializationStrategy, USDKKotlinx_serialization_runtimeDecoder, USDKKotlinx_serialization_runtimeDeserializationStrategy, USDKConsentInterface, USDKKtor_utilsStringValues, USDKKotlinMapEntry, USDKKtor_httpHeaders, USDKKotlinIterator, USDKKotlinx_serialization_runtimeCompositeEncoder, USDKKotlinx_serialization_runtimeSerialModule, USDKKotlinAnnotation, USDKKotlinx_serialization_runtimeCompositeDecoder, USDKKotlinCoroutineContextKey, USDKKotlinCoroutineContextElement, USDKKotlinCoroutineContext, USDKKotlinContinuation, USDKKotlinContinuationInterceptor, USDKKotlinx_coroutines_coreRunnable, USDKKotlinx_serialization_runtimeSerialModuleCollector, USDKKotlinKClass, USDKKotlinKDeclarationContainer, USDKKotlinKAnnotatedElement, USDKKotlinKClassifier;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wnullability"

__attribute__((swift_name("KotlinBase")))
@interface USDKBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface USDKBase (USDKBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface USDKMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface USDKMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorUSDKKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface USDKNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface USDKByte : USDKNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface USDKUByte : USDKNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface USDKShort : USDKNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface USDKUShort : USDKNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface USDKInt : USDKNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface USDKUInt : USDKNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface USDKLong : USDKNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface USDKULong : USDKNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface USDKFloat : USDKNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface USDKDouble : USDKNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface USDKBoolean : USDKNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR")))
@interface USDKMR : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)mR __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.fonts")))
@interface USDKMRFonts : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)fonts __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.images")))
@interface USDKMRImages : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)images __attribute__((swift_name("init()")));
@property (readonly) USDKResourcesImageResource *logo __attribute__((swift_name("logo")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.plurals")))
@interface USDKMRPlurals : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)plurals __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MR.strings")))
@interface USDKMRStrings : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)strings __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Storage")))
@interface USDKStorage : USDKBase
- (instancetype)initWithName:(NSString * _Nullable)name context:(id)context __attribute__((swift_name("init(name:context:)"))) __attribute__((objc_designated_initializer));
- (void)deleteKeyKey:(NSString *)key __attribute__((swift_name("deleteKey(key:)")));
- (NSString * _Nullable)getValueKey:(NSString *)key defaultValue:(NSString * _Nullable)defaultValue __attribute__((swift_name("getValue(key:defaultValue:)")));
- (void)putValueKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("putValue(key:value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserDecision")))
@interface USDKUserDecision : USDKBase
- (instancetype)initWithServiceId:(NSString *)serviceId status:(BOOL)status __attribute__((swift_name("init(serviceId:status:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (USDKUserDecision *)doCopyServiceId:(NSString *)serviceId status:(BOOL)status __attribute__((swift_name("doCopy(serviceId:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *serviceId __attribute__((swift_name("serviceId")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserOptions")))
@interface USDKUserOptions : USDKBase
- (instancetype)initWithControllerId:(NSString * _Nullable)controllerId language:(NSString * _Nullable)language version:(NSString * _Nullable)version debugMode:(USDKBoolean * _Nullable)debugMode __attribute__((swift_name("init(controllerId:language:version:debugMode:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (USDKBoolean * _Nullable)component4 __attribute__((swift_name("component4()")));
- (USDKUserOptions *)doCopyControllerId:(NSString * _Nullable)controllerId language:(NSString * _Nullable)language version:(NSString * _Nullable)version debugMode:(USDKBoolean * _Nullable)debugMode __attribute__((swift_name("doCopy(controllerId:language:version:debugMode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable controllerId __attribute__((swift_name("controllerId")));
@property (readonly) USDKBoolean * _Nullable debugMode __attribute__((swift_name("debugMode")));
@property (readonly) NSString * _Nullable language __attribute__((swift_name("language")));
@property (readonly) NSString * _Nullable version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Usercentrics")))
@interface USDKUsercentrics : USDKBase
- (instancetype)initWithSettingsId:(NSString *)settingsId options:(USDKUserOptions * _Nullable)options __attribute__((swift_name("init(settingsId:options:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithSettingsId:(NSString *)settingsId options:(USDKUserOptions * _Nullable)options androidContext:(id)androidContext __attribute__((swift_name("init(settingsId:options:androidContext:)"))) __attribute__((objc_designated_initializer));
- (void)acceptAllServicesConsentType:(USDKCONSENT_TYPE *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKKotlinThrowable *))onFailure __attribute__((swift_name("acceptAllServices(consentType:callback:onFailure:)")));
- (void)acceptAllServicesInCategoryCategorySlug:(NSString *)categorySlug consentType:(USDKCONSENT_TYPE *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKKotlinThrowable *))onFailure __attribute__((swift_name("acceptAllServicesInCategory(categorySlug:consentType:callback:onFailure:)")));
- (void)changeLanguageLanguage:(NSString *)language callback:(void (^)(void))callback onFailure:(void (^)(USDKKotlinThrowable *))onFailure __attribute__((swift_name("changeLanguage(language:callback:onFailure:)")));
- (void)denyAllServicesConsentType:(USDKCONSENT_TYPE *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKKotlinThrowable *))onFailure __attribute__((swift_name("denyAllServices(consentType:callback:onFailure:)")));
- (void)denyAllServicesInCategoryCategorySlug:(NSString *)categorySlug consentType:(USDKCONSENT_TYPE *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKKotlinThrowable *))onFailure __attribute__((swift_name("denyAllServicesInCategory(categorySlug:consentType:callback:onFailure:)")));
- (NSArray<USDKCategory *> * _Nullable)getCategories __attribute__((swift_name("getCategories()")));
- (NSString * _Nullable)getControllerId __attribute__((swift_name("getControllerId()")));
- (NSString *)getLanguage __attribute__((swift_name("getLanguage()")));
- (NSArray<USDKService *> *)getServices __attribute__((swift_name("getServices()")));
- (USDKSettings * _Nullable)getSettings __attribute__((swift_name("getSettings()")));
- (id _Nullable)getUIViewDismissView:(void (^)(void))dismissView __attribute__((swift_name("getUIView(dismissView:)")));
- (void)initializeCallback:(void (^)(USDKUsercentricsINITIAL_VIEW *))callback onFailure:(void (^)(USDKKotlinThrowable *))onFailure __attribute__((swift_name("initialize(callback:onFailure:)")));
- (void)subscribeEventEventName:(NSString *)eventName action:(void (^)(USDKEvent *))action __attribute__((swift_name("subscribeEvent(eventName:action:)")));
- (void)updateServicesDecisions:(NSArray<USDKUserDecision *> *)decisions consentType:(USDKCONSENT_TYPE *)consentType callback:(void (^)(void))callback onFailure:(void (^)(USDKKotlinThrowable *))onFailure __attribute__((swift_name("updateServices(decisions:consentType:callback:onFailure:)")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol USDKKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface USDKKotlinEnum : USDKBase <USDKKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(USDKKotlinEnum *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Usercentrics.INITIAL_VIEW")))
@interface USDKUsercentricsINITIAL_VIEW : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKUsercentricsINITIAL_VIEW *firstLayer __attribute__((swift_name("firstLayer")));
@property (class, readonly) USDKUsercentricsINITIAL_VIEW *none __attribute__((swift_name("none")));
- (int32_t)compareToOther:(USDKUsercentricsINITIAL_VIEW *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("ViewControllerCustom")))
@interface USDKViewControllerCustom : NSObject
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentStatusObject")))
@interface USDKConsentStatusObject : USDKBase
- (instancetype)initWithName:(NSString *)name status:(USDKBoolean * _Nullable)status __attribute__((swift_name("init(name:status:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (USDKBoolean * _Nullable)component2 __attribute__((swift_name("component2()")));
- (USDKConsentStatusObject *)doCopyName:(NSString *)name status:(USDKBoolean * _Nullable)status __attribute__((swift_name("doCopy(name:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) USDKBoolean * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Event")))
@interface USDKEvent : USDKBase
- (instancetype)initWithName:(NSString *)name type:(USDKCONSENT_TYPE * _Nullable)type services:(NSArray<USDKConsentStatusObject *> *)services __attribute__((swift_name("init(name:type:services:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (USDKCONSENT_TYPE * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSArray<USDKConsentStatusObject *> *)component3 __attribute__((swift_name("component3()")));
- (USDKEvent *)doCopyName:(NSString *)name type:(USDKCONSENT_TYPE * _Nullable)type services:(NSArray<USDKConsentStatusObject *> *)services __attribute__((swift_name("doCopy(name:type:services:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSArray<USDKConsentStatusObject *> *services __attribute__((swift_name("services")));
@property (readonly) USDKCONSENT_TYPE * _Nullable type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventManager")))
@interface USDKEventManager : USDKBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)notifyEvent:(USDKEvent *)event __attribute__((swift_name("notify(event:)")));
- (void)registerEventName:(NSString *)eventName action:(void (^)(USDKEvent *))action __attribute__((swift_name("register(eventName:action:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OptionalOptions")))
@interface USDKOptionalOptions : USDKBase
- (instancetype)initWithCredentials:(id)credentials headers:(USDKKtor_httpHeadersBuilder *)headers mode:(id)mode __attribute__((swift_name("init(credentials:headers:mode:)"))) __attribute__((objc_designated_initializer));
- (id)component1 __attribute__((swift_name("component1()")));
- (USDKKtor_httpHeadersBuilder *)component2 __attribute__((swift_name("component2()")));
- (id)component3 __attribute__((swift_name("component3()")));
- (USDKOptionalOptions *)doCopyCredentials:(id)credentials headers:(USDKKtor_httpHeadersBuilder *)headers mode:(id)mode __attribute__((swift_name("doCopy(credentials:headers:mode:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id credentials __attribute__((swift_name("credentials")));
@property (readonly) USDKKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@property (readonly) id mode __attribute__((swift_name("mode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OptionalOptions.Companion")))
@interface USDKOptionalOptionsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants")))
@interface USDKConstants : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) NSString *RESOURCE_NOT_FOUND __attribute__((swift_name("RESOURCE_NOT_FOUND")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Language")))
@interface USDKLanguage : USDKBase
- (instancetype)initWithApi:(USDKApi *)api storage:(USDKDeviceStorage *)storage debug:(void (^)(NSString *))debug __attribute__((swift_name("init(api:storage:debug:)"))) __attribute__((objc_designated_initializer));
- (void)doInitLanguage:(NSString *)language __attribute__((swift_name("doInit(language:)")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
- (void)resolveLanguageCallback:(void (^)(void))callback onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("resolveLanguage(callback:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WarningMessages")))
@interface USDKWarningMessages : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKWarningMessages *defaultDeviceLanguage __attribute__((swift_name("defaultDeviceLanguage")));
@property (class, readonly) USDKWarningMessages *defaultFirstAvailableLanguage __attribute__((swift_name("defaultFirstAvailableLanguage")));
- (int32_t)compareToOther:(USDKWarningMessages *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants_")))
@interface USDKConstants_ : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) USDKKotlinArray *EU_COUNTRIES __attribute__((swift_name("EU_COUNTRIES")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CountryData")))
@interface USDKCountryData : USDKBase
- (instancetype)initWithCountryCode:(NSString *)countryCode countryName:(NSString *)countryName __attribute__((swift_name("init(countryCode:countryName:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKCountryData *)doCopyCountryCode:(NSString *)countryCode countryName:(NSString *)countryName __attribute__((swift_name("doCopy(countryCode:countryName:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *countryCode __attribute__((swift_name("countryCode")));
@property (readonly) NSString *countryName __attribute__((swift_name("countryName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CountryData.Companion")))
@interface USDKCountryDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Location")))
@interface USDKLocation : USDKBase
- (instancetype)initWithApi:(USDKApi *)api debug:(void (^)(NSString *))debug __attribute__((swift_name("init(api:debug:)"))) __attribute__((objc_designated_initializer));
- (void)getIsUserInEUCallback:(void (^)(USDKBoolean *))callback onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("getIsUserInEU(callback:onError:)")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LocationData")))
@interface USDKLocationData : USDKBase
- (instancetype)initWithClientLocation:(USDKCountryData *)clientLocation __attribute__((swift_name("init(clientLocation:)"))) __attribute__((objc_designated_initializer));
- (USDKCountryData *)component1 __attribute__((swift_name("component1()")));
- (USDKLocationData *)doCopyClientLocation:(USDKCountryData *)clientLocation __attribute__((swift_name("doCopy(clientLocation:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKCountryData *clientLocation __attribute__((swift_name("clientLocation")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LocationData.Companion")))
@interface USDKLocationDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserCountry")))
@interface USDKUserCountry : USDKBase
- (instancetype)initWithCode:(NSString *)code name:(NSString *)name __attribute__((swift_name("init(code:name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKUserCountry *)doCopyCode:(NSString *)code name:(NSString *)name __attribute__((swift_name("doCopy(code:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *code __attribute__((swift_name("code")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserCountry.Companion")))
@interface USDKUserCountryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BaseService")))
@interface USDKBaseService : USDKBase
- (instancetype)initWithDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description id:(NSString *)id language:(USDKLanguage_ *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (USDKProcessingCompany *)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (NSArray<NSString *> *)component12 __attribute__((swift_name("component12()")));
- (USDKURLs *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (USDKDataDistribution *)component2 __attribute__((swift_name("component2()")));
- (NSArray<NSString *> *)component3 __attribute__((swift_name("component3()")));
- (NSArray<NSString *> *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (USDKLanguage_ *)component7 __attribute__((swift_name("component7()")));
- (NSArray<NSString *> *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (USDKBaseService *)doCopyDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description id:(NSString *)id language:(USDKLanguage_ *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version __attribute__((swift_name("doCopy(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> *dataCollected __attribute__((swift_name("dataCollected")));
@property (readonly) USDKDataDistribution *dataDistribution __attribute__((swift_name("dataDistribution")));
@property (readonly) NSArray<NSString *> *dataPurposes __attribute__((swift_name("dataPurposes")));
@property (readonly) NSArray<NSString *> *dataRecipients __attribute__((swift_name("dataRecipients")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) USDKLanguage_ *language __attribute__((swift_name("language")));
@property (readonly) NSArray<NSString *> *legalBasis __attribute__((swift_name("legalBasis")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) USDKProcessingCompany *processingCompany __attribute__((swift_name("processingCompany")));
@property (readonly) NSString *retentionPeriodDescription __attribute__((swift_name("retentionPeriodDescription")));
@property (readonly) NSArray<NSString *> *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property (readonly) USDKURLs *urls __attribute__((swift_name("urls")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BaseService.Companion")))
@interface USDKBaseServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Button")))
@interface USDKButton : USDKBase
- (instancetype)initWithIsEnabled:(USDKBoolean * _Nullable)isEnabled label:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("init(isEnabled:label:url:)"))) __attribute__((objc_designated_initializer));
- (USDKBoolean * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (USDKButton *)doCopyIsEnabled:(USDKBoolean * _Nullable)isEnabled label:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("doCopy(isEnabled:label:url:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKBoolean * _Nullable isEnabled __attribute__((swift_name("isEnabled")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@property (readonly) NSString * _Nullable url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Button.Companion")))
@interface USDKButtonCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Buttons")))
@interface USDKButtons : USDKBase
- (instancetype)initWithAcceptAll:(USDKButton *)acceptAll denyAll:(USDKButton *)denyAll save:(USDKButton *)save showSecondLayer:(USDKButton *)showSecondLayer __attribute__((swift_name("init(acceptAll:denyAll:save:showSecondLayer:)"))) __attribute__((objc_designated_initializer));
- (USDKButton *)component1 __attribute__((swift_name("component1()")));
- (USDKButton *)component2 __attribute__((swift_name("component2()")));
- (USDKButton *)component3 __attribute__((swift_name("component3()")));
- (USDKButton *)component4 __attribute__((swift_name("component4()")));
- (USDKButtons *)doCopyAcceptAll:(USDKButton *)acceptAll denyAll:(USDKButton *)denyAll save:(USDKButton *)save showSecondLayer:(USDKButton *)showSecondLayer __attribute__((swift_name("doCopy(acceptAll:denyAll:save:showSecondLayer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKButton *acceptAll __attribute__((swift_name("acceptAll")));
@property (readonly) USDKButton *denyAll __attribute__((swift_name("denyAll")));
@property (readonly) USDKButton *save __attribute__((swift_name("save")));
@property (readonly) USDKButton *showSecondLayer __attribute__((swift_name("showSecondLayer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Buttons.Companion")))
@interface USDKButtonsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CONSENT_STATUS")))
@interface USDKCONSENT_STATUS : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKCONSENT_STATUS *false_ __attribute__((swift_name("false_")));
@property (class, readonly) USDKCONSENT_STATUS *true_ __attribute__((swift_name("true_")));
- (int32_t)compareToOther:(USDKCONSENT_STATUS *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Category")))
@interface USDKCategory : USDKBase
- (instancetype)initWithDescription:(NSString *)description isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label services:(NSArray<USDKService *> *)services slug:(NSString *)slug __attribute__((swift_name("init(description:isEssential:isHidden:label:services:slug:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSArray<USDKService *> *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (USDKCategory *)doCopyDescription:(NSString *)description isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label services:(NSArray<USDKService *> *)services slug:(NSString *)slug __attribute__((swift_name("doCopy(description:isEssential:isHidden:label:services:slug:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property BOOL isEssential __attribute__((swift_name("isEssential")));
@property BOOL isHidden __attribute__((swift_name("isHidden")));
@property NSString *label __attribute__((swift_name("label")));
@property NSArray<USDKService *> *services __attribute__((swift_name("services")));
@property NSString *slug __attribute__((swift_name("slug")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent")))
@interface USDKConsent : USDKBase
- (instancetype)initWithHistory:(NSArray<USDKConsentHistory *> *)history status:(BOOL)status __attribute__((swift_name("init(history:status:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKConsentHistory *> *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (USDKConsent *)doCopyHistory:(NSArray<USDKConsentHistory *> *)history status:(BOOL)status __attribute__((swift_name("doCopy(history:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKConsentHistory *> *history __attribute__((swift_name("history")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent.Companion")))
@interface USDKConsentCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants__")))
@interface USDKConstants__ : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) NSString *CATEGORY_NONE __attribute__((swift_name("CATEGORY_NONE")));
@property (readonly) NSString *FIRST_LAYER_SERVICE_ID __attribute__((swift_name("FIRST_LAYER_SERVICE_ID")));
@property (readonly) NSString *USERCENTRICS_URL __attribute__((swift_name("USERCENTRICS_URL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DATA_EXCHANGE_TYPE")))
@interface USDKDATA_EXCHANGE_TYPE : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKDATA_EXCHANGE_TYPE *dataLayer __attribute__((swift_name("dataLayer")));
@property (class, readonly) USDKDATA_EXCHANGE_TYPE *windowEvent __attribute__((swift_name("windowEvent")));
- (int32_t)compareToOther:(USDKDATA_EXCHANGE_TYPE *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistribution")))
@interface USDKDataDistribution : USDKBase
- (instancetype)initWithProcessingLocation:(NSString *)processingLocation thirdPartyCountries:(NSString *)thirdPartyCountries __attribute__((swift_name("init(processingLocation:thirdPartyCountries:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKDataDistribution *)doCopyProcessingLocation:(NSString *)processingLocation thirdPartyCountries:(NSString *)thirdPartyCountries __attribute__((swift_name("doCopy(processingLocation:thirdPartyCountries:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *processingLocation __attribute__((swift_name("processingLocation")));
@property (readonly) NSString *thirdPartyCountries __attribute__((swift_name("thirdPartyCountries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistribution.Companion")))
@interface USDKDataDistributionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistributionTitle")))
@interface USDKDataDistributionTitle : USDKBase
- (instancetype)initWithProcessingLocationTitle:(NSString *)processingLocationTitle thirdPartyCountriesTitle:(NSString *)thirdPartyCountriesTitle __attribute__((swift_name("init(processingLocationTitle:thirdPartyCountriesTitle:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKDataDistributionTitle *)doCopyProcessingLocationTitle:(NSString *)processingLocationTitle thirdPartyCountriesTitle:(NSString *)thirdPartyCountriesTitle __attribute__((swift_name("doCopy(processingLocationTitle:thirdPartyCountriesTitle:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *processingLocationTitle __attribute__((swift_name("processingLocationTitle")));
@property (readonly) NSString *thirdPartyCountriesTitle __attribute__((swift_name("thirdPartyCountriesTitle")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataDistributionTitle.Companion")))
@interface USDKDataDistributionTitleCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataExchangeSetting")))
@interface USDKDataExchangeSetting : USDKBase
- (instancetype)initWithNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("init(names:type:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (USDKDataExchangeSetting *)doCopyNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("doCopy(names:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<NSString *> *names __attribute__((swift_name("names")));
@property int32_t type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DescriptionTitle")))
@interface USDKDescriptionTitle : USDKBase
- (instancetype)initWithDescription:(NSString *)description title:(NSString *)title __attribute__((swift_name("init(description:title:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKDescriptionTitle *)doCopyDescription:(NSString *)description title:(NSString *)title __attribute__((swift_name("doCopy(description:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DescriptionTitle.Companion")))
@interface USDKDescriptionTitleCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ExtendedSettings")))
@interface USDKExtendedSettings : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId id:(NSString *)id ui:(USDKUISettings * _Nullable)ui version:(NSString *)version acceptAllImplicitlyOutsideEU:(USDKBoolean * _Nullable)acceptAllImplicitlyOutsideEU categories:(NSArray<USDKCategory *> *)categories dataExchangeSettings:(NSArray<USDKDataExchangeSetting *> *)dataExchangeSettings showFirstLayerOnVersionChange:(NSArray<USDKInt *> *)showFirstLayerOnVersionChange __attribute__((swift_name("init(controllerId:id:ui:version:acceptAllImplicitlyOutsideEU:categories:dataExchangeSettings:showFirstLayerOnVersionChange:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKUISettings * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (USDKBoolean * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSArray<USDKCategory *> *)component6 __attribute__((swift_name("component6()")));
- (NSArray<USDKDataExchangeSetting *> *)component7 __attribute__((swift_name("component7()")));
- (NSArray<USDKInt *> *)component8 __attribute__((swift_name("component8()")));
- (USDKExtendedSettings *)doCopyControllerId:(NSString *)controllerId id:(NSString *)id ui:(USDKUISettings * _Nullable)ui version:(NSString *)version acceptAllImplicitlyOutsideEU:(USDKBoolean * _Nullable)acceptAllImplicitlyOutsideEU categories:(NSArray<USDKCategory *> *)categories dataExchangeSettings:(NSArray<USDKDataExchangeSetting *> *)dataExchangeSettings showFirstLayerOnVersionChange:(NSArray<USDKInt *> *)showFirstLayerOnVersionChange __attribute__((swift_name("doCopy(controllerId:id:ui:version:acceptAllImplicitlyOutsideEU:categories:dataExchangeSettings:showFirstLayerOnVersionChange:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKBoolean * _Nullable acceptAllImplicitlyOutsideEU __attribute__((swift_name("acceptAllImplicitlyOutsideEU")));
@property NSArray<USDKCategory *> *categories __attribute__((swift_name("categories")));
@property NSString *controllerId __attribute__((swift_name("controllerId")));
@property NSArray<USDKDataExchangeSetting *> *dataExchangeSettings __attribute__((swift_name("dataExchangeSettings")));
@property NSString *id __attribute__((swift_name("id")));
@property NSArray<USDKInt *> *showFirstLayerOnVersionChange __attribute__((swift_name("showFirstLayerOnVersionChange")));
@property USDKUISettings * _Nullable ui __attribute__((swift_name("ui")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayer")))
@interface USDKFirstLayer : USDKBase
- (instancetype)initWithDescription:(USDKFirstLayerDescription *)description isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title __attribute__((swift_name("init(description:isOverlayEnabled:title:)"))) __attribute__((objc_designated_initializer));
- (USDKFirstLayerDescription *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (USDKFirstLayer *)doCopyDescription:(USDKFirstLayerDescription *)description isOverlayEnabled:(BOOL)isOverlayEnabled title:(NSString *)title __attribute__((swift_name("doCopy(description:isOverlayEnabled:title:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) USDKFirstLayerDescription *description __attribute__((swift_name("description")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayer.Companion")))
@interface USDKFirstLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayerDescription")))
@interface USDKFirstLayerDescription : USDKBase
- (instancetype)initWithDefault:(NSString *)default_ short:(NSString * _Nullable)short_ __attribute__((swift_name("init(default:short:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (USDKFirstLayerDescription *)doCopyDefault:(NSString *)default_ short:(NSString * _Nullable)short_ __attribute__((swift_name("doCopy(default:short:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=default) NSString *default_ __attribute__((swift_name("default_")));
@property (readonly, getter=short) NSString * _Nullable short_ __attribute__((swift_name("short_")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FirstLayerDescription.Companion")))
@interface USDKFirstLayerDescriptionCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GeneralLabels")))
@interface USDKGeneralLabels : USDKBase
- (instancetype)initWithConsentGiven:(NSString *)consentGiven consentNotGiven:(NSString *)consentNotGiven consentType:(NSString *)consentType controllerId:(NSString *)controllerId copy:(NSString *)copy date:(NSString *)date decision:(NSString *)decision explicit:(NSString *)explicit_ implicit:(NSString *)implicit processorId:(NSString *)processorId showMore:(NSString *)showMore __attribute__((swift_name("init(consentGiven:consentNotGiven:consentType:controllerId:copy:date:decision:explicit:implicit:processorId:showMore:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString *)component7 __attribute__((swift_name("component7()")));
- (NSString *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (USDKGeneralLabels *)doCopyConsentGiven:(NSString *)consentGiven consentNotGiven:(NSString *)consentNotGiven consentType:(NSString *)consentType controllerId:(NSString *)controllerId copy:(NSString *)copy date:(NSString *)date decision:(NSString *)decision explicit:(NSString *)explicit_ implicit:(NSString *)implicit processorId:(NSString *)processorId showMore:(NSString *)showMore __attribute__((swift_name("doCopy(consentGiven:consentNotGiven:consentType:controllerId:copy:date:decision:explicit:implicit:processorId:showMore:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *consentGiven __attribute__((swift_name("consentGiven")));
@property (readonly) NSString *consentNotGiven __attribute__((swift_name("consentNotGiven")));
@property (readonly) NSString *consentType __attribute__((swift_name("consentType")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly, getter=doCopy) NSString *copy __attribute__((swift_name("copy")));
@property (readonly) NSString *date __attribute__((swift_name("date")));
@property (readonly) NSString *decision __attribute__((swift_name("decision")));
@property (readonly, getter=explicit) NSString *explicit_ __attribute__((swift_name("explicit_")));
@property (readonly) NSString *implicit __attribute__((swift_name("implicit")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) NSString *showMore __attribute__((swift_name("showMore")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GeneralLabels.Companion")))
@interface USDKGeneralLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LEGACY_BACKGROUND_OVERLAY_TARGET")))
@interface USDKLEGACY_BACKGROUND_OVERLAY_TARGET : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKLEGACY_BACKGROUND_OVERLAY_TARGET *firstLayer __attribute__((swift_name("firstLayer")));
@property (class, readonly) USDKLEGACY_BACKGROUND_OVERLAY_TARGET *secondLayer __attribute__((swift_name("secondLayer")));
- (int32_t)compareToOther:(USDKLEGACY_BACKGROUND_OVERLAY_TARGET *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t i __attribute__((swift_name("i")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LEGACY_DATA_EXCHANGE_TYPE")))
@interface USDKLEGACY_DATA_EXCHANGE_TYPE : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKLEGACY_DATA_EXCHANGE_TYPE *dataLayer __attribute__((swift_name("dataLayer")));
@property (class, readonly) USDKLEGACY_DATA_EXCHANGE_TYPE *windowEvent __attribute__((swift_name("windowEvent")));
@property (class, readonly) USDKLEGACY_DATA_EXCHANGE_TYPE *unknown __attribute__((swift_name("unknown")));
- (int32_t)compareToOther:(USDKLEGACY_DATA_EXCHANGE_TYPE *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) int32_t i __attribute__((swift_name("i")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LEGACY_STRING_TO_LIST_PROPERTIES")))
@interface USDKLEGACY_STRING_TO_LIST_PROPERTIES : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKLEGACY_STRING_TO_LIST_PROPERTIES *dataCollectedList __attribute__((swift_name("dataCollectedList")));
@property (class, readonly) USDKLEGACY_STRING_TO_LIST_PROPERTIES *dataPurposesList __attribute__((swift_name("dataPurposesList")));
@property (class, readonly) USDKLEGACY_STRING_TO_LIST_PROPERTIES *dataRecipientsList __attribute__((swift_name("dataRecipientsList")));
@property (class, readonly) USDKLEGACY_STRING_TO_LIST_PROPERTIES *technologyUsed __attribute__((swift_name("technologyUsed")));
- (int32_t)compareToOther:(USDKLEGACY_STRING_TO_LIST_PROPERTIES *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *s __attribute__((swift_name("s")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LEGACY_VERSION")))
@interface USDKLEGACY_VERSION : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKLEGACY_VERSION *major __attribute__((swift_name("major")));
@property (class, readonly) USDKLEGACY_VERSION *minor __attribute__((swift_name("minor")));
@property (class, readonly) USDKLEGACY_VERSION *patch __attribute__((swift_name("patch")));
- (int32_t)compareToOther:(USDKLEGACY_VERSION *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *s __attribute__((swift_name("s")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Labels")))
@interface USDKLabels : USDKBase
- (instancetype)initWithGeneral:(USDKGeneralLabels *)general service:(USDKServiceLabels *)service __attribute__((swift_name("init(general:service:)"))) __attribute__((objc_designated_initializer));
- (USDKGeneralLabels *)component1 __attribute__((swift_name("component1()")));
- (USDKServiceLabels *)component2 __attribute__((swift_name("component2()")));
- (USDKLabels *)doCopyGeneral:(USDKGeneralLabels *)general service:(USDKServiceLabels *)service __attribute__((swift_name("doCopy(general:service:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKGeneralLabels *general __attribute__((swift_name("general")));
@property (readonly) USDKServiceLabels *service __attribute__((swift_name("service")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Labels.Companion")))
@interface USDKLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Language_")))
@interface USDKLanguage_ : USDKBase
- (instancetype)initWithAvailable:(NSArray<NSString *> *)available isSelectorEnabled:(USDKBoolean * _Nullable)isSelectorEnabled selected:(NSString *)selected __attribute__((swift_name("init(available:isSelectorEnabled:selected:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (USDKBoolean * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (USDKLanguage_ *)doCopyAvailable:(NSArray<NSString *> *)available isSelectorEnabled:(USDKBoolean * _Nullable)isSelectorEnabled selected:(NSString *)selected __attribute__((swift_name("doCopy(available:isSelectorEnabled:selected:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> *available __attribute__((swift_name("available")));
@property (readonly) USDKBoolean * _Nullable isSelectorEnabled __attribute__((swift_name("isSelectorEnabled")));
@property (readonly) NSString *selected __attribute__((swift_name("selected")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Language_.Companion")))
@interface USDKLanguage_Companion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyBackgroundOverlay")))
@interface USDKLegacyBackgroundOverlay : USDKBase
- (instancetype)initWithDarken:(int32_t)darken target:(NSArray<USDKInt *> *)target __attribute__((swift_name("init(darken:target:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSArray<USDKInt *> *)component2 __attribute__((swift_name("component2()")));
- (USDKLegacyBackgroundOverlay *)doCopyDarken:(int32_t)darken target:(NSArray<USDKInt *> *)target __attribute__((swift_name("doCopy(darken:target:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property int32_t darken __attribute__((swift_name("darken")));
@property NSArray<USDKInt *> *target __attribute__((swift_name("target")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyBackgroundOverlay.Companion")))
@interface USDKLegacyBackgroundOverlayCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("LegacyBaseServiceInterface")))
@protocol USDKLegacyBaseServiceInterface
@required
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyBaseService")))
@interface USDKLegacyBaseService : USDKBase <USDKLegacyBaseServiceInterface>
- (instancetype)initWithDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version __attribute__((swift_name("init(description:templateId:version:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (USDKLegacyBaseService *)doCopyDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version __attribute__((swift_name("doCopy(description:templateId:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyBaseService.Companion")))
@interface USDKLegacyBaseServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyCategory")))
@interface USDKLegacyCategory : USDKBase
- (instancetype)initWithCategorySlug:(NSString *)categorySlug description:(NSString *)description isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label __attribute__((swift_name("init(categorySlug:description:isEssential:isHidden:label:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (BOOL)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (USDKLegacyCategory *)doCopyCategorySlug:(NSString *)categorySlug description:(NSString *)description isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden label:(NSString *)label __attribute__((swift_name("doCopy(categorySlug:description:isEssential:isHidden:label:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *categorySlug __attribute__((swift_name("categorySlug")));
@property (getter=description_) NSString *description __attribute__((swift_name("description")));
@property BOOL isEssential __attribute__((swift_name("isEssential")));
@property BOOL isHidden __attribute__((swift_name("isHidden")));
@property NSString *label __attribute__((swift_name("label")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyCategory.Companion")))
@interface USDKLegacyCategoryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyDataExchangeSetting")))
@interface USDKLegacyDataExchangeSetting : USDKBase
- (instancetype)initWithNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("init(names:type:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (USDKLegacyDataExchangeSetting *)doCopyNames:(NSArray<NSString *> *)names type:(int32_t)type __attribute__((swift_name("doCopy(names:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<NSString *> *names __attribute__((swift_name("names")));
@property int32_t type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyDataExchangeSetting.Companion")))
@interface USDKLegacyDataExchangeSettingCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyHashService")))
@interface USDKLegacyHashService : USDKBase <USDKLegacyBaseServiceInterface>
- (instancetype)initWithDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version addressOfProcessingCompany:(NSString *)addressOfProcessingCompany cookiePolicyURL:(NSString *)cookiePolicyURL dataCollectedList:(NSArray<NSString *> *)dataCollectedList dataProtectionOfficer:(NSString *)dataProtectionOfficer dataProcessor:(NSString * _Nullable)dataProcessor dataProcessors:(NSArray<NSString *> *)dataProcessors dataPurposes:(NSArray<NSString *> *)dataPurposes dataPurposesList:(NSArray<NSString *> *)dataPurposesList dataRecipientsList:(NSArray<NSString *> *)dataRecipientsList descriptionOfService:(NSString *)descriptionOfService language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable legalBasisList:(NSArray<NSString *> *)legalBasisList legalGround:(NSString *)legalGround linkToDpa:(NSString *)linkToDpa locationOfProcessing:(NSString *)locationOfProcessing nameOfProcessingCompany:(NSString *)nameOfProcessingCompany optOutUrl:(NSString *)optOutUrl policyOfProcessorUrl:(NSString *)policyOfProcessorUrl privacyPolicyURL:(NSString *)privacyPolicyURL processingCompany:(NSString *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription retentionPeriodList:(NSArray<NSString *> *)retentionPeriodList technologyUsed:(NSArray<NSString *> *)technologyUsed thirdCountryTransfer:(NSString *)thirdCountryTransfer __attribute__((swift_name("init(description:templateId:version:addressOfProcessingCompany:cookiePolicyURL:dataCollectedList:dataProtectionOfficer:dataProcessor:dataProcessors:dataPurposes:dataPurposesList:dataRecipientsList:descriptionOfService:language:languagesAvailable:legalBasisList:legalGround:linkToDpa:locationOfProcessing:nameOfProcessingCompany:optOutUrl:policyOfProcessorUrl:privacyPolicyURL:processingCompany:retentionPeriodDescription:retentionPeriodList:technologyUsed:thirdCountryTransfer:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> *)component10 __attribute__((swift_name("component10()")));
- (NSArray<NSString *> *)component11 __attribute__((swift_name("component11()")));
- (NSArray<NSString *> *)component12 __attribute__((swift_name("component12()")));
- (NSString *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSArray<NSString *> *)component15 __attribute__((swift_name("component15()")));
- (NSArray<NSString *> *)component16 __attribute__((swift_name("component16()")));
- (NSString *)component17 __attribute__((swift_name("component17()")));
- (NSString *)component18 __attribute__((swift_name("component18()")));
- (NSString *)component19 __attribute__((swift_name("component19()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component20 __attribute__((swift_name("component20()")));
- (NSString *)component21 __attribute__((swift_name("component21()")));
- (NSString *)component22 __attribute__((swift_name("component22()")));
- (NSString *)component23 __attribute__((swift_name("component23()")));
- (NSString *)component24 __attribute__((swift_name("component24()")));
- (NSString *)component25 __attribute__((swift_name("component25()")));
- (NSArray<NSString *> *)component26 __attribute__((swift_name("component26()")));
- (NSArray<NSString *> *)component27 __attribute__((swift_name("component27()")));
- (NSString *)component28 __attribute__((swift_name("component28()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSArray<NSString *> *)component6 __attribute__((swift_name("component6()")));
- (NSString *)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSArray<NSString *> *)component9 __attribute__((swift_name("component9()")));
- (USDKLegacyHashService *)doCopyDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version addressOfProcessingCompany:(NSString *)addressOfProcessingCompany cookiePolicyURL:(NSString *)cookiePolicyURL dataCollectedList:(NSArray<NSString *> *)dataCollectedList dataProtectionOfficer:(NSString *)dataProtectionOfficer dataProcessor:(NSString * _Nullable)dataProcessor dataProcessors:(NSArray<NSString *> *)dataProcessors dataPurposes:(NSArray<NSString *> *)dataPurposes dataPurposesList:(NSArray<NSString *> *)dataPurposesList dataRecipientsList:(NSArray<NSString *> *)dataRecipientsList descriptionOfService:(NSString *)descriptionOfService language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable legalBasisList:(NSArray<NSString *> *)legalBasisList legalGround:(NSString *)legalGround linkToDpa:(NSString *)linkToDpa locationOfProcessing:(NSString *)locationOfProcessing nameOfProcessingCompany:(NSString *)nameOfProcessingCompany optOutUrl:(NSString *)optOutUrl policyOfProcessorUrl:(NSString *)policyOfProcessorUrl privacyPolicyURL:(NSString *)privacyPolicyURL processingCompany:(NSString *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription retentionPeriodList:(NSArray<NSString *> *)retentionPeriodList technologyUsed:(NSArray<NSString *> *)technologyUsed thirdCountryTransfer:(NSString *)thirdCountryTransfer __attribute__((swift_name("doCopy(description:templateId:version:addressOfProcessingCompany:cookiePolicyURL:dataCollectedList:dataProtectionOfficer:dataProcessor:dataProcessors:dataPurposes:dataPurposesList:dataRecipientsList:descriptionOfService:language:languagesAvailable:legalBasisList:legalGround:linkToDpa:locationOfProcessing:nameOfProcessingCompany:optOutUrl:policyOfProcessorUrl:privacyPolicyURL:processingCompany:retentionPeriodDescription:retentionPeriodList:technologyUsed:thirdCountryTransfer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *addressOfProcessingCompany __attribute__((swift_name("addressOfProcessingCompany")));
@property NSString *cookiePolicyURL __attribute__((swift_name("cookiePolicyURL")));
@property NSArray<NSString *> *dataCollectedList __attribute__((swift_name("dataCollectedList")));
@property NSString * _Nullable dataProcessor __attribute__((swift_name("dataProcessor")));
@property NSArray<NSString *> *dataProcessors __attribute__((swift_name("dataProcessors")));
@property NSString *dataProtectionOfficer __attribute__((swift_name("dataProtectionOfficer")));
@property NSArray<NSString *> *dataPurposes __attribute__((swift_name("dataPurposes")));
@property NSArray<NSString *> *dataPurposesList __attribute__((swift_name("dataPurposesList")));
@property NSArray<NSString *> *dataRecipientsList __attribute__((swift_name("dataRecipientsList")));
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property NSString *descriptionOfService __attribute__((swift_name("descriptionOfService")));
@property NSString *language __attribute__((swift_name("language")));
@property NSArray<NSString *> *languagesAvailable __attribute__((swift_name("languagesAvailable")));
@property NSArray<NSString *> *legalBasisList __attribute__((swift_name("legalBasisList")));
@property NSString *legalGround __attribute__((swift_name("legalGround")));
@property NSString *linkToDpa __attribute__((swift_name("linkToDpa")));
@property NSString *locationOfProcessing __attribute__((swift_name("locationOfProcessing")));
@property NSString *nameOfProcessingCompany __attribute__((swift_name("nameOfProcessingCompany")));
@property NSString *optOutUrl __attribute__((swift_name("optOutUrl")));
@property NSString *policyOfProcessorUrl __attribute__((swift_name("policyOfProcessorUrl")));
@property NSString *privacyPolicyURL __attribute__((swift_name("privacyPolicyURL")));
@property NSString *processingCompany __attribute__((swift_name("processingCompany")));
@property NSString *retentionPeriodDescription __attribute__((swift_name("retentionPeriodDescription")));
@property NSArray<NSString *> *retentionPeriodList __attribute__((swift_name("retentionPeriodList")));
@property NSArray<NSString *> *technologyUsed __attribute__((swift_name("technologyUsed")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *thirdCountryTransfer __attribute__((swift_name("thirdCountryTransfer")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyHashService.Companion")))
@interface USDKLegacyHashServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyLabels")))
@interface USDKLegacyLabels : USDKBase
- (instancetype)initWithBtnAccept:(NSString *)btnAccept btnDeny:(NSString *)btnDeny btnMoreInfo:(NSString *)btnMoreInfo btnSave:(NSString *)btnSave categories:(NSString *)categories cookiePolicyInfo:(NSString *)cookiePolicyInfo copy:(NSString *)copy dataCollectedInfo:(NSString *)dataCollectedInfo dataCollectedList:(NSString *)dataCollectedList dataRecipientsList:(NSString *)dataRecipientsList dataPurposes:(NSString *)dataPurposes dataPurposesInfo:(NSString *)dataPurposesInfo date:(NSString *)date descriptionOfService:(NSString *)descriptionOfService furtherInformationOptOut:(NSString *)furtherInformationOptOut headerCenterSecondary:(NSString *)headerCenterSecondary history:(NSString *)history imprintLinkText:(NSString *)imprintLinkText legalBasisList:(NSString *)legalBasisList legalBasisInfo:(NSString *)legalBasisInfo linkToDpaInfo:(NSString *)linkToDpaInfo locationOfProcessing:(NSString *)locationOfProcessing partnerPoweredByLinkText:(NSString *)partnerPoweredByLinkText policyOf:(NSString *)policyOf poweredBy:(NSString *)poweredBy privacyPolicyLinkText:(NSString *)privacyPolicyLinkText processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriod:(NSString *)retentionPeriod technologiesUsed:(NSString *)technologiesUsed technologiesUsedInfo:(NSString *)technologiesUsedInfo titleCenterSecondary:(NSString *)titleCenterSecondary transferToThirdCountries:(NSString *)transferToThirdCountries __attribute__((swift_name("init(btnAccept:btnDeny:btnMoreInfo:btnSave:categories:cookiePolicyInfo:copy:dataCollectedInfo:dataCollectedList:dataRecipientsList:dataPurposes:dataPurposesInfo:date:descriptionOfService:furtherInformationOptOut:headerCenterSecondary:history:imprintLinkText:legalBasisList:legalBasisInfo:linkToDpaInfo:locationOfProcessing:partnerPoweredByLinkText:policyOf:poweredBy:privacyPolicyLinkText:processingCompanyTitle:retentionPeriod:technologiesUsed:technologiesUsedInfo:titleCenterSecondary:transferToThirdCountries:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (NSString *)component12 __attribute__((swift_name("component12()")));
- (NSString *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component15 __attribute__((swift_name("component15()")));
- (NSString *)component16 __attribute__((swift_name("component16()")));
- (NSString *)component17 __attribute__((swift_name("component17()")));
- (NSString *)component18 __attribute__((swift_name("component18()")));
- (NSString *)component19 __attribute__((swift_name("component19()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component20 __attribute__((swift_name("component20()")));
- (NSString *)component21 __attribute__((swift_name("component21()")));
- (NSString *)component22 __attribute__((swift_name("component22()")));
- (NSString *)component23 __attribute__((swift_name("component23()")));
- (NSString *)component24 __attribute__((swift_name("component24()")));
- (NSString *)component25 __attribute__((swift_name("component25()")));
- (NSString *)component26 __attribute__((swift_name("component26()")));
- (NSString *)component27 __attribute__((swift_name("component27()")));
- (NSString *)component28 __attribute__((swift_name("component28()")));
- (NSString *)component29 __attribute__((swift_name("component29()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component30 __attribute__((swift_name("component30()")));
- (NSString *)component31 __attribute__((swift_name("component31()")));
- (NSString *)component32 __attribute__((swift_name("component32()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString *)component7 __attribute__((swift_name("component7()")));
- (NSString *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (USDKLegacyLabels *)doCopyBtnAccept:(NSString *)btnAccept btnDeny:(NSString *)btnDeny btnMoreInfo:(NSString *)btnMoreInfo btnSave:(NSString *)btnSave categories:(NSString *)categories cookiePolicyInfo:(NSString *)cookiePolicyInfo copy:(NSString *)copy dataCollectedInfo:(NSString *)dataCollectedInfo dataCollectedList:(NSString *)dataCollectedList dataRecipientsList:(NSString *)dataRecipientsList dataPurposes:(NSString *)dataPurposes dataPurposesInfo:(NSString *)dataPurposesInfo date:(NSString *)date descriptionOfService:(NSString *)descriptionOfService furtherInformationOptOut:(NSString *)furtherInformationOptOut headerCenterSecondary:(NSString *)headerCenterSecondary history:(NSString *)history imprintLinkText:(NSString *)imprintLinkText legalBasisList:(NSString *)legalBasisList legalBasisInfo:(NSString *)legalBasisInfo linkToDpaInfo:(NSString *)linkToDpaInfo locationOfProcessing:(NSString *)locationOfProcessing partnerPoweredByLinkText:(NSString *)partnerPoweredByLinkText policyOf:(NSString *)policyOf poweredBy:(NSString *)poweredBy privacyPolicyLinkText:(NSString *)privacyPolicyLinkText processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriod:(NSString *)retentionPeriod technologiesUsed:(NSString *)technologiesUsed technologiesUsedInfo:(NSString *)technologiesUsedInfo titleCenterSecondary:(NSString *)titleCenterSecondary transferToThirdCountries:(NSString *)transferToThirdCountries __attribute__((swift_name("doCopy(btnAccept:btnDeny:btnMoreInfo:btnSave:categories:cookiePolicyInfo:copy:dataCollectedInfo:dataCollectedList:dataRecipientsList:dataPurposes:dataPurposesInfo:date:descriptionOfService:furtherInformationOptOut:headerCenterSecondary:history:imprintLinkText:legalBasisList:legalBasisInfo:linkToDpaInfo:locationOfProcessing:partnerPoweredByLinkText:policyOf:poweredBy:privacyPolicyLinkText:processingCompanyTitle:retentionPeriod:technologiesUsed:technologiesUsedInfo:titleCenterSecondary:transferToThirdCountries:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *btnAccept __attribute__((swift_name("btnAccept")));
@property NSString *btnDeny __attribute__((swift_name("btnDeny")));
@property NSString *btnMoreInfo __attribute__((swift_name("btnMoreInfo")));
@property NSString *btnSave __attribute__((swift_name("btnSave")));
@property NSString *categories __attribute__((swift_name("categories")));
@property NSString *cookiePolicyInfo __attribute__((swift_name("cookiePolicyInfo")));
@property (getter=doCopy) NSString *copy __attribute__((swift_name("copy")));
@property NSString *dataCollectedInfo __attribute__((swift_name("dataCollectedInfo")));
@property NSString *dataCollectedList __attribute__((swift_name("dataCollectedList")));
@property NSString *dataPurposes __attribute__((swift_name("dataPurposes")));
@property NSString *dataPurposesInfo __attribute__((swift_name("dataPurposesInfo")));
@property NSString *dataRecipientsList __attribute__((swift_name("dataRecipientsList")));
@property NSString *date __attribute__((swift_name("date")));
@property NSString *descriptionOfService __attribute__((swift_name("descriptionOfService")));
@property NSString *furtherInformationOptOut __attribute__((swift_name("furtherInformationOptOut")));
@property NSString *headerCenterSecondary __attribute__((swift_name("headerCenterSecondary")));
@property NSString *history __attribute__((swift_name("history")));
@property NSString *imprintLinkText __attribute__((swift_name("imprintLinkText")));
@property NSString *legalBasisInfo __attribute__((swift_name("legalBasisInfo")));
@property NSString *legalBasisList __attribute__((swift_name("legalBasisList")));
@property NSString *linkToDpaInfo __attribute__((swift_name("linkToDpaInfo")));
@property NSString *locationOfProcessing __attribute__((swift_name("locationOfProcessing")));
@property NSString *partnerPoweredByLinkText __attribute__((swift_name("partnerPoweredByLinkText")));
@property NSString *policyOf __attribute__((swift_name("policyOf")));
@property NSString *poweredBy __attribute__((swift_name("poweredBy")));
@property NSString *privacyPolicyLinkText __attribute__((swift_name("privacyPolicyLinkText")));
@property NSString *processingCompanyTitle __attribute__((swift_name("processingCompanyTitle")));
@property NSString *retentionPeriod __attribute__((swift_name("retentionPeriod")));
@property NSString *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property NSString *technologiesUsedInfo __attribute__((swift_name("technologiesUsedInfo")));
@property NSString *titleCenterSecondary __attribute__((swift_name("titleCenterSecondary")));
@property NSString *transferToThirdCountries __attribute__((swift_name("transferToThirdCountries")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyLabels.Companion")))
@interface USDKLegacyLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyService")))
@interface USDKLegacyService : USDKBase <USDKLegacyBaseServiceInterface>
- (instancetype)initWithDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version categorySlug:(NSString *)categorySlug defaultConsentStatus:(BOOL)defaultConsentStatus isDeactivated:(BOOL)isDeactivated isHidden:(BOOL)isHidden subConsents:(NSArray<USDKLegacyBaseService *> *)subConsents __attribute__((swift_name("init(description:templateId:version:categorySlug:defaultConsentStatus:isDeactivated:isHidden:subConsents:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (BOOL)component5 __attribute__((swift_name("component5()")));
- (BOOL)component6 __attribute__((swift_name("component6()")));
- (BOOL)component7 __attribute__((swift_name("component7()")));
- (NSArray<USDKLegacyBaseService *> *)component8 __attribute__((swift_name("component8()")));
- (USDKLegacyService *)doCopyDescription:(NSString * _Nullable)description templateId:(NSString *)templateId version:(NSString *)version categorySlug:(NSString *)categorySlug defaultConsentStatus:(BOOL)defaultConsentStatus isDeactivated:(BOOL)isDeactivated isHidden:(BOOL)isHidden subConsents:(NSArray<USDKLegacyBaseService *> *)subConsents __attribute__((swift_name("doCopy(description:templateId:version:categorySlug:defaultConsentStatus:isDeactivated:isHidden:subConsents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *categorySlug __attribute__((swift_name("categorySlug")));
@property BOOL defaultConsentStatus __attribute__((swift_name("defaultConsentStatus")));
@property (getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property BOOL isDeactivated __attribute__((swift_name("isDeactivated")));
@property BOOL isHidden __attribute__((swift_name("isHidden")));
@property NSArray<USDKLegacyBaseService *> *subConsents __attribute__((swift_name("subConsents")));
@property NSString *templateId __attribute__((swift_name("templateId")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyService.Companion")))
@interface USDKLegacyServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacySettings")))
@interface USDKLegacySettings : USDKBase
- (instancetype)initWithShowInitialViewForVersionChange:(NSArray<NSString *> *)showInitialViewForVersionChange backgroundOverlay:(NSArray<USDKLegacyBackgroundOverlay *> *)backgroundOverlay bannerMessage:(NSString *)bannerMessage bannerMobileDescription:(NSString * _Nullable)bannerMobileDescription bannerMobileDescriptionIsActive:(BOOL)bannerMobileDescriptionIsActive btnDenyIsVisible:(BOOL)btnDenyIsVisible btnMoreInfoActionSelection:(int32_t)btnMoreInfoActionSelection btnMoreInfoIsVisible:(BOOL)btnMoreInfoIsVisible categories:(NSArray<USDKLegacyCategory *> *)categories consentTemplates:(NSArray<USDKLegacyService *> *)consentTemplates dataExchangeOnPage:(NSArray<USDKLegacyDataExchangeSetting *> *)dataExchangeOnPage displayOnlyForEU:(BOOL)displayOnlyForEU enablePoweredBy:(BOOL)enablePoweredBy imprintUrl:(NSString * _Nullable)imprintUrl labels:(USDKLegacyLabels *)labels language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable moreInfoButtonUrl:(NSString * _Nullable)moreInfoButtonUrl partnerPoweredByUrl:(NSString * _Nullable)partnerPoweredByUrl privacyButtonIsVisible:(BOOL)privacyButtonIsVisible privacyPolicyUrl:(NSString * _Nullable)privacyPolicyUrl settingsId:(NSString *)settingsId showLanguageDropdown:(BOOL)showLanguageDropdown version:(NSString *)version __attribute__((swift_name("init(showInitialViewForVersionChange:backgroundOverlay:bannerMessage:bannerMobileDescription:bannerMobileDescriptionIsActive:btnDenyIsVisible:btnMoreInfoActionSelection:btnMoreInfoIsVisible:categories:consentTemplates:dataExchangeOnPage:displayOnlyForEU:enablePoweredBy:imprintUrl:labels:language:languagesAvailable:moreInfoButtonUrl:partnerPoweredByUrl:privacyButtonIsVisible:privacyPolicyUrl:settingsId:showLanguageDropdown:version:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<USDKLegacyService *> *)component10 __attribute__((swift_name("component10()")));
- (NSArray<USDKLegacyDataExchangeSetting *> *)component11 __attribute__((swift_name("component11()")));
- (BOOL)component12 __attribute__((swift_name("component12()")));
- (BOOL)component13 __attribute__((swift_name("component13()")));
- (NSString * _Nullable)component14 __attribute__((swift_name("component14()")));
- (USDKLegacyLabels *)component15 __attribute__((swift_name("component15()")));
- (NSString *)component16 __attribute__((swift_name("component16()")));
- (NSArray<NSString *> *)component17 __attribute__((swift_name("component17()")));
- (NSString * _Nullable)component18 __attribute__((swift_name("component18()")));
- (NSString * _Nullable)component19 __attribute__((swift_name("component19()")));
- (NSArray<USDKLegacyBackgroundOverlay *> *)component2 __attribute__((swift_name("component2()")));
- (BOOL)component20 __attribute__((swift_name("component20()")));
- (NSString * _Nullable)component21 __attribute__((swift_name("component21()")));
- (NSString *)component22 __attribute__((swift_name("component22()")));
- (BOOL)component23 __attribute__((swift_name("component23()")));
- (NSString *)component24 __attribute__((swift_name("component24()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (BOOL)component5 __attribute__((swift_name("component5()")));
- (BOOL)component6 __attribute__((swift_name("component6()")));
- (int32_t)component7 __attribute__((swift_name("component7()")));
- (BOOL)component8 __attribute__((swift_name("component8()")));
- (NSArray<USDKLegacyCategory *> *)component9 __attribute__((swift_name("component9()")));
- (USDKLegacySettings *)doCopyShowInitialViewForVersionChange:(NSArray<NSString *> *)showInitialViewForVersionChange backgroundOverlay:(NSArray<USDKLegacyBackgroundOverlay *> *)backgroundOverlay bannerMessage:(NSString *)bannerMessage bannerMobileDescription:(NSString * _Nullable)bannerMobileDescription bannerMobileDescriptionIsActive:(BOOL)bannerMobileDescriptionIsActive btnDenyIsVisible:(BOOL)btnDenyIsVisible btnMoreInfoActionSelection:(int32_t)btnMoreInfoActionSelection btnMoreInfoIsVisible:(BOOL)btnMoreInfoIsVisible categories:(NSArray<USDKLegacyCategory *> *)categories consentTemplates:(NSArray<USDKLegacyService *> *)consentTemplates dataExchangeOnPage:(NSArray<USDKLegacyDataExchangeSetting *> *)dataExchangeOnPage displayOnlyForEU:(BOOL)displayOnlyForEU enablePoweredBy:(BOOL)enablePoweredBy imprintUrl:(NSString * _Nullable)imprintUrl labels:(USDKLegacyLabels *)labels language:(NSString *)language languagesAvailable:(NSArray<NSString *> *)languagesAvailable moreInfoButtonUrl:(NSString * _Nullable)moreInfoButtonUrl partnerPoweredByUrl:(NSString * _Nullable)partnerPoweredByUrl privacyButtonIsVisible:(BOOL)privacyButtonIsVisible privacyPolicyUrl:(NSString * _Nullable)privacyPolicyUrl settingsId:(NSString *)settingsId showLanguageDropdown:(BOOL)showLanguageDropdown version:(NSString *)version __attribute__((swift_name("doCopy(showInitialViewForVersionChange:backgroundOverlay:bannerMessage:bannerMobileDescription:bannerMobileDescriptionIsActive:btnDenyIsVisible:btnMoreInfoActionSelection:btnMoreInfoIsVisible:categories:consentTemplates:dataExchangeOnPage:displayOnlyForEU:enablePoweredBy:imprintUrl:labels:language:languagesAvailable:moreInfoButtonUrl:partnerPoweredByUrl:privacyButtonIsVisible:privacyPolicyUrl:settingsId:showLanguageDropdown:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSArray<USDKLegacyBackgroundOverlay *> *backgroundOverlay __attribute__((swift_name("backgroundOverlay")));
@property NSString *bannerMessage __attribute__((swift_name("bannerMessage")));
@property NSString * _Nullable bannerMobileDescription __attribute__((swift_name("bannerMobileDescription")));
@property BOOL bannerMobileDescriptionIsActive __attribute__((swift_name("bannerMobileDescriptionIsActive")));
@property BOOL btnDenyIsVisible __attribute__((swift_name("btnDenyIsVisible")));
@property int32_t btnMoreInfoActionSelection __attribute__((swift_name("btnMoreInfoActionSelection")));
@property BOOL btnMoreInfoIsVisible __attribute__((swift_name("btnMoreInfoIsVisible")));
@property NSArray<USDKLegacyCategory *> *categories __attribute__((swift_name("categories")));
@property NSArray<USDKLegacyService *> *consentTemplates __attribute__((swift_name("consentTemplates")));
@property NSArray<USDKLegacyDataExchangeSetting *> *dataExchangeOnPage __attribute__((swift_name("dataExchangeOnPage")));
@property BOOL displayOnlyForEU __attribute__((swift_name("displayOnlyForEU")));
@property BOOL enablePoweredBy __attribute__((swift_name("enablePoweredBy")));
@property NSString * _Nullable imprintUrl __attribute__((swift_name("imprintUrl")));
@property USDKLegacyLabels *labels __attribute__((swift_name("labels")));
@property NSString *language __attribute__((swift_name("language")));
@property NSArray<NSString *> *languagesAvailable __attribute__((swift_name("languagesAvailable")));
@property NSString * _Nullable moreInfoButtonUrl __attribute__((swift_name("moreInfoButtonUrl")));
@property NSString * _Nullable partnerPoweredByUrl __attribute__((swift_name("partnerPoweredByUrl")));
@property BOOL privacyButtonIsVisible __attribute__((swift_name("privacyButtonIsVisible")));
@property NSString * _Nullable privacyPolicyUrl __attribute__((swift_name("privacyPolicyUrl")));
@property NSString *settingsId __attribute__((swift_name("settingsId")));
@property NSArray<NSString *> *showInitialViewForVersionChange __attribute__((swift_name("showInitialViewForVersionChange")));
@property BOOL showLanguageDropdown __attribute__((swift_name("showLanguageDropdown")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacySettings.Companion")))
@interface USDKLegacySettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerializationStrategy")))
@protocol USDKKotlinx_serialization_runtimeSerializationStrategy
@required
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeDeserializationStrategy")))
@protocol USDKKotlinx_serialization_runtimeDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (id _Nullable)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(id _Nullable)old __attribute__((swift_name("patch(decoder:old:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeKSerializer")))
@protocol USDKKotlinx_serialization_runtimeKSerializer <USDKKotlinx_serialization_runtimeSerializationStrategy, USDKKotlinx_serialization_runtimeDeserializationStrategy>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LegacyVersionSerializer")))
@interface USDKLegacyVersionSerializer : USDKBase <USDKKotlinx_serialization_runtimeKSerializer>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)legacyVersionSerializer __attribute__((swift_name("init()")));
- (USDKLEGACY_VERSION *)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (USDKLEGACY_VERSION *)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(USDKLEGACY_VERSION *)old __attribute__((swift_name("patch(decoder:old:)")));
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(USDKLEGACY_VERSION *)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Link")))
@interface USDKLink : USDKBase
- (instancetype)initWithLabel:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("init(label:url:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (USDKLink *)doCopyLabel:(NSString *)label url:(NSString * _Nullable)url __attribute__((swift_name("doCopy(label:url:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@property (readonly) NSString * _Nullable url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Link.Companion")))
@interface USDKLinkCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Links")))
@interface USDKLinks : USDKBase
- (instancetype)initWithCookiePolicy:(USDKLink *)cookiePolicy imprint:(USDKLink *)imprint privacyPolicy:(USDKLink *)privacyPolicy __attribute__((swift_name("init(cookiePolicy:imprint:privacyPolicy:)"))) __attribute__((objc_designated_initializer));
- (USDKLink *)component1 __attribute__((swift_name("component1()")));
- (USDKLink *)component2 __attribute__((swift_name("component2()")));
- (USDKLink *)component3 __attribute__((swift_name("component3()")));
- (USDKLinks *)doCopyCookiePolicy:(USDKLink *)cookiePolicy imprint:(USDKLink *)imprint privacyPolicy:(USDKLink *)privacyPolicy __attribute__((swift_name("doCopy(cookiePolicy:imprint:privacyPolicy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKLink *cookiePolicy __attribute__((swift_name("cookiePolicy")));
@property (readonly) USDKLink *imprint __attribute__((swift_name("imprint")));
@property (readonly) USDKLink *privacyPolicy __attribute__((swift_name("privacyPolicy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Links.Companion")))
@interface USDKLinksCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PoweredBy")))
@interface USDKPoweredBy : USDKBase
- (instancetype)initWithIsEnabled:(BOOL)isEnabled label:(NSString *)label partnerUrl:(NSString * _Nullable)partnerUrl partnerUrlLabel:(NSString * _Nullable)partnerUrlLabel url:(NSString *)url urlLabel:(NSString *)urlLabel __attribute__((swift_name("init(isEnabled:label:partnerUrl:partnerUrlLabel:url:urlLabel:)"))) __attribute__((objc_designated_initializer));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (USDKPoweredBy *)doCopyIsEnabled:(BOOL)isEnabled label:(NSString *)label partnerUrl:(NSString * _Nullable)partnerUrl partnerUrlLabel:(NSString * _Nullable)partnerUrlLabel url:(NSString *)url urlLabel:(NSString *)urlLabel __attribute__((swift_name("doCopy(isEnabled:label:partnerUrl:partnerUrlLabel:url:urlLabel:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isEnabled __attribute__((swift_name("isEnabled")));
@property (readonly) NSString *label __attribute__((swift_name("label")));
@property (readonly) NSString * _Nullable partnerUrl __attribute__((swift_name("partnerUrl")));
@property (readonly) NSString * _Nullable partnerUrlLabel __attribute__((swift_name("partnerUrlLabel")));
@property (readonly) NSString *url __attribute__((swift_name("url")));
@property (readonly) NSString *urlLabel __attribute__((swift_name("urlLabel")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PoweredBy.Companion")))
@interface USDKPoweredByCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProcessingCompany")))
@interface USDKProcessingCompany : USDKBase
- (instancetype)initWithAddress:(NSString *)address dataProtectionOfficer:(NSString *)dataProtectionOfficer name:(NSString *)name __attribute__((swift_name("init(address:dataProtectionOfficer:name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (USDKProcessingCompany *)doCopyAddress:(NSString *)address dataProtectionOfficer:(NSString *)dataProtectionOfficer name:(NSString *)name __attribute__((swift_name("doCopy(address:dataProtectionOfficer:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@property (readonly) NSString *dataProtectionOfficer __attribute__((swift_name("dataProtectionOfficer")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProcessingCompany.Companion")))
@interface USDKProcessingCompanyCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SecondLayer")))
@interface USDKSecondLayer : USDKBase
- (instancetype)initWithTitle:(NSString *)title description:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs __attribute__((swift_name("init(title:description:isOverlayEnabled:tabs:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (USDKTabs *)component4 __attribute__((swift_name("component4()")));
- (USDKSecondLayer *)doCopyTitle:(NSString *)title description:(NSString *)description isOverlayEnabled:(BOOL)isOverlayEnabled tabs:(USDKTabs *)tabs __attribute__((swift_name("doCopy(title:description:isOverlayEnabled:tabs:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) BOOL isOverlayEnabled __attribute__((swift_name("isOverlayEnabled")));
@property (readonly) USDKTabs *tabs __attribute__((swift_name("tabs")));
@property (readonly) NSString *title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SecondLayer.Companion")))
@interface USDKSecondLayerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Service")))
@interface USDKService : USDKBase
- (instancetype)initWithDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description id:(NSString *)id language:(USDKLanguage_ *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version categorySlug:(NSString *)categorySlug consent:(USDKConsent *)consent isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden processorId:(NSString *)processorId subServices:(NSArray<USDKBaseService *> *)subServices __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:categorySlug:consent:isEssential:isHidden:processorId:subServices:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (USDKProcessingCompany *)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (NSArray<NSString *> *)component12 __attribute__((swift_name("component12()")));
- (USDKURLs *)component13 __attribute__((swift_name("component13()")));
- (NSString *)component14 __attribute__((swift_name("component14()")));
- (NSString *)component15 __attribute__((swift_name("component15()")));
- (USDKConsent *)component16 __attribute__((swift_name("component16()")));
- (BOOL)component17 __attribute__((swift_name("component17()")));
- (BOOL)component18 __attribute__((swift_name("component18()")));
- (NSString *)component19 __attribute__((swift_name("component19()")));
- (USDKDataDistribution *)component2 __attribute__((swift_name("component2()")));
- (NSArray<USDKBaseService *> *)component20 __attribute__((swift_name("component20()")));
- (NSArray<NSString *> *)component3 __attribute__((swift_name("component3()")));
- (NSArray<NSString *> *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (USDKLanguage_ *)component7 __attribute__((swift_name("component7()")));
- (NSArray<NSString *> *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (USDKService *)doCopyDataCollected:(NSArray<NSString *> *)dataCollected dataDistribution:(USDKDataDistribution *)dataDistribution dataPurposes:(NSArray<NSString *> *)dataPurposes dataRecipients:(NSArray<NSString *> *)dataRecipients description:(NSString *)description id:(NSString *)id language:(USDKLanguage_ *)language legalBasis:(NSArray<NSString *> *)legalBasis name:(NSString *)name processingCompany:(USDKProcessingCompany *)processingCompany retentionPeriodDescription:(NSString *)retentionPeriodDescription technologiesUsed:(NSArray<NSString *> *)technologiesUsed urls:(USDKURLs *)urls version:(NSString *)version categorySlug:(NSString *)categorySlug consent:(USDKConsent *)consent isEssential:(BOOL)isEssential isHidden:(BOOL)isHidden processorId:(NSString *)processorId subServices:(NSArray<USDKBaseService *> *)subServices __attribute__((swift_name("doCopy(dataCollected:dataDistribution:dataPurposes:dataRecipients:description:id:language:legalBasis:name:processingCompany:retentionPeriodDescription:technologiesUsed:urls:version:categorySlug:consent:isEssential:isHidden:processorId:subServices:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *categorySlug __attribute__((swift_name("categorySlug")));
@property (readonly) USDKConsent *consent __attribute__((swift_name("consent")));
@property (readonly) NSArray<NSString *> *dataCollected __attribute__((swift_name("dataCollected")));
@property (readonly) USDKDataDistribution *dataDistribution __attribute__((swift_name("dataDistribution")));
@property (readonly) NSArray<NSString *> *dataPurposes __attribute__((swift_name("dataPurposes")));
@property (readonly) NSArray<NSString *> *dataRecipients __attribute__((swift_name("dataRecipients")));
@property (readonly, getter=description_) NSString *description __attribute__((swift_name("description")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) BOOL isEssential __attribute__((swift_name("isEssential")));
@property (readonly) BOOL isHidden __attribute__((swift_name("isHidden")));
@property (readonly) USDKLanguage_ *language __attribute__((swift_name("language")));
@property (readonly) NSArray<NSString *> *legalBasis __attribute__((swift_name("legalBasis")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) USDKProcessingCompany *processingCompany __attribute__((swift_name("processingCompany")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) NSString *retentionPeriodDescription __attribute__((swift_name("retentionPeriodDescription")));
@property (readonly) NSArray<USDKBaseService *> *subServices __attribute__((swift_name("subServices")));
@property (readonly) NSArray<NSString *> *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property (readonly) USDKURLs *urls __attribute__((swift_name("urls")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Service.Companion")))
@interface USDKServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceHashArrayObject")))
@interface USDKServiceHashArrayObject : USDKBase
- (instancetype)initWithId:(NSString *)id language:(NSString *)language version:(NSString *)version __attribute__((swift_name("init(id:language:version:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (USDKServiceHashArrayObject *)doCopyId:(NSString *)id language:(NSString *)language version:(NSString *)version __attribute__((swift_name("doCopy(id:language:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *id __attribute__((swift_name("id")));
@property NSString *language __attribute__((swift_name("language")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceHashArrayObject.Companion")))
@interface USDKServiceHashArrayObjectCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceLabels")))
@interface USDKServiceLabels : USDKBase
- (instancetype)initWithDataCollected:(USDKDescriptionTitle *)dataCollected dataDistribution:(USDKDataDistributionTitle *)dataDistribution dataPurposes:(USDKDescriptionTitle *)dataPurposes dataRecipientsTitle:(NSString *)dataRecipientsTitle descriptionTitle:(NSString *)descriptionTitle history:(USDKDescriptionTitle *)history legalBasis:(USDKDescriptionTitle *)legalBasis processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriodTitle:(NSString *)retentionPeriodTitle technologiesUsed:(USDKDescriptionTitle *)technologiesUsed urls:(USDKURLsTitle *)urls __attribute__((swift_name("init(dataCollected:dataDistribution:dataPurposes:dataRecipientsTitle:descriptionTitle:history:legalBasis:processingCompanyTitle:retentionPeriodTitle:technologiesUsed:urls:)"))) __attribute__((objc_designated_initializer));
- (USDKDescriptionTitle *)component1 __attribute__((swift_name("component1()")));
- (USDKDescriptionTitle *)component10 __attribute__((swift_name("component10()")));
- (USDKURLsTitle *)component11 __attribute__((swift_name("component11()")));
- (USDKDataDistributionTitle *)component2 __attribute__((swift_name("component2()")));
- (USDKDescriptionTitle *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (USDKDescriptionTitle *)component6 __attribute__((swift_name("component6()")));
- (USDKDescriptionTitle *)component7 __attribute__((swift_name("component7()")));
- (NSString *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (USDKServiceLabels *)doCopyDataCollected:(USDKDescriptionTitle *)dataCollected dataDistribution:(USDKDataDistributionTitle *)dataDistribution dataPurposes:(USDKDescriptionTitle *)dataPurposes dataRecipientsTitle:(NSString *)dataRecipientsTitle descriptionTitle:(NSString *)descriptionTitle history:(USDKDescriptionTitle *)history legalBasis:(USDKDescriptionTitle *)legalBasis processingCompanyTitle:(NSString *)processingCompanyTitle retentionPeriodTitle:(NSString *)retentionPeriodTitle technologiesUsed:(USDKDescriptionTitle *)technologiesUsed urls:(USDKURLsTitle *)urls __attribute__((swift_name("doCopy(dataCollected:dataDistribution:dataPurposes:dataRecipientsTitle:descriptionTitle:history:legalBasis:processingCompanyTitle:retentionPeriodTitle:technologiesUsed:urls:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKDescriptionTitle *dataCollected __attribute__((swift_name("dataCollected")));
@property (readonly) USDKDataDistributionTitle *dataDistribution __attribute__((swift_name("dataDistribution")));
@property (readonly) USDKDescriptionTitle *dataPurposes __attribute__((swift_name("dataPurposes")));
@property (readonly) NSString *dataRecipientsTitle __attribute__((swift_name("dataRecipientsTitle")));
@property (readonly) NSString *descriptionTitle __attribute__((swift_name("descriptionTitle")));
@property (readonly) USDKDescriptionTitle *history __attribute__((swift_name("history")));
@property (readonly) USDKDescriptionTitle *legalBasis __attribute__((swift_name("legalBasis")));
@property (readonly) NSString *processingCompanyTitle __attribute__((swift_name("processingCompanyTitle")));
@property (readonly) NSString *retentionPeriodTitle __attribute__((swift_name("retentionPeriodTitle")));
@property (readonly) USDKDescriptionTitle *technologiesUsed __attribute__((swift_name("technologiesUsed")));
@property (readonly) USDKURLsTitle *urls __attribute__((swift_name("urls")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceLabels.Companion")))
@interface USDKServiceLabelsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Settings")))
@interface USDKSettings : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId id:(NSString *)id ui:(USDKUISettings *)ui version:(NSString *)version __attribute__((swift_name("init(controllerId:id:ui:version:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKUISettings *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (USDKSettings *)doCopyControllerId:(NSString *)controllerId id:(NSString *)id ui:(USDKUISettings *)ui version:(NSString *)version __attribute__((swift_name("doCopy(controllerId:id:ui:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *controllerId __attribute__((swift_name("controllerId")));
@property NSString *id __attribute__((swift_name("id")));
@property USDKUISettings *ui __attribute__((swift_name("ui")));
@property NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SettingsService")))
@interface USDKSettingsService : USDKBase
- (instancetype)initWithApi:(USDKApi *)api location:(USDKLocation *)location debug:(void (^)(NSString *))debug __attribute__((swift_name("init(api:location:debug:)"))) __attribute__((objc_designated_initializer));
- (USDKSettings *)getBaseSettings __attribute__((swift_name("getBaseSettings()")));
- (NSArray<USDKCategory *> *)getCategories __attribute__((swift_name("getCategories()")));
- (USDKCategory * _Nullable)getCategoryBySlugCategorySlug:(NSString *)categorySlug __attribute__((swift_name("getCategoryBySlug(categorySlug:)")));
- (NSArray<USDKDataExchangeSetting *> * _Nullable)getDataExchangeSettings __attribute__((swift_name("getDataExchangeSettings()")));
- (NSArray<USDKCategory *> *)getEssentialCategories __attribute__((swift_name("getEssentialCategories()")));
- (NSArray<USDKCategory *> *)getNonEssentialCategories __attribute__((swift_name("getNonEssentialCategories()")));
- (NSArray<USDKService *> *)getServices __attribute__((swift_name("getServices()")));
- (NSArray<USDKService *> *)getServicesByIdsIds:(NSArray<NSString *> *)ids __attribute__((swift_name("getServicesByIds(ids:)")));
- (NSArray<USDKService *> *)getServicesFromCategoriesCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("getServicesFromCategories(categories:)")));
- (NSArray<USDKService *> *)getServicesWithConsent __attribute__((swift_name("getServicesWithConsent()")));
- (USDKExtendedSettings *)getSettings __attribute__((swift_name("getSettings()")));
- (void)doInitSettingsControllerID:(NSString * _Nullable)controllerID callback:(void (^)(void))callback onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("doInitSettings(controllerID:callback:onError:)")));
- (NSArray<USDKCategory *> *)mergeServicesIntoExistingCategoriesUpdatedServices:(NSArray<USDKService *> *)updatedServices __attribute__((swift_name("mergeServicesIntoExistingCategories(updatedServices:)")));
- (USDKExtendedSettings *)removeEmptyCategoriesSettings:(USDKExtendedSettings *)settings __attribute__((swift_name("removeEmptyCategories(settings:)")));
- (NSArray<USDKCategory *> *)removeNoneCategoryCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("removeNoneCategory(categories:)")));
- (void)setCategoriesCategories:(NSArray<USDKCategory *> *)categories __attribute__((swift_name("setCategories(categories:)")));
- (void)setSettingsSettings:(USDKExtendedSettings *)settings __attribute__((swift_name("setSettings(settings:)")));
- (void)shouldAcceptAllImplicitlyOnInitCallback:(void (^)(USDKBoolean *))callback onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("shouldAcceptAllImplicitlyOnInit(callback:onError:)")));
- (BOOL)shouldShowFirstLayerOnVersionChangeStorageVersion:(NSString *)storageVersion __attribute__((swift_name("shouldShowFirstLayerOnVersionChange(storageVersion:)")));
- (NSArray<USDKService *> *)updateServicesWithConsentServices:(NSArray<USDKService *> *)services status:(USDKCONSENT_STATUS *)status __attribute__((swift_name("updateServicesWithConsent(services:status:)")));
- (NSArray<USDKService *> *)updateServicesWithConsentsServices:(NSArray<USDKService *> *)services userDecisions:(NSArray<USDKUserDecision *> *)userDecisions __attribute__((swift_name("updateServicesWithConsents(services:userDecisions:)")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeJsonTransformingSerializer")))
@interface USDKKotlinx_serialization_runtimeJsonTransformingSerializer : USDKBase <USDKKotlinx_serialization_runtimeKSerializer>
- (instancetype)initWithTSerializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)tSerializer transformationName:(NSString *)transformationName __attribute__((swift_name("init(tSerializer:transformationName:)"))) __attribute__((objc_designated_initializer));
- (id)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (id)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(id)old __attribute__((swift_name("patch(decoder:old:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)readTransformElement:(USDKKotlinx_serialization_runtimeJsonElement *)element __attribute__((swift_name("readTransform(element:)")));
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(id)value __attribute__((swift_name("serialize(encoder:value:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)writeTransformElement:(USDKKotlinx_serialization_runtimeJsonElement *)element __attribute__((swift_name("writeTransform(element:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StringOrListSerializer")))
@interface USDKStringOrListSerializer : USDKKotlinx_serialization_runtimeJsonTransformingSerializer
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithTSerializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)tSerializer transformationName:(NSString *)transformationName __attribute__((swift_name("init(tSerializer:transformationName:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)stringOrListSerializer __attribute__((swift_name("init()")));
- (NSArray<NSString *> *)deserializeDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (NSArray<NSString *> *)patchDecoder:(id<USDKKotlinx_serialization_runtimeDecoder>)decoder old:(NSArray<NSString *> *)old __attribute__((swift_name("patch(decoder:old:)")));
- (USDKKotlinx_serialization_runtimeJsonElement *)readTransformElement:(USDKKotlinx_serialization_runtimeJsonElement *)element __attribute__((swift_name("readTransform(element:)")));
- (void)serializeEncoder:(id<USDKKotlinx_serialization_runtimeEncoder>)encoder value:(NSArray<NSString *> *)value __attribute__((swift_name("serialize(encoder:value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Tabs")))
@interface USDKTabs : USDKBase
- (instancetype)initWithCategories:(USDKButton *)categories services:(USDKButton *)services __attribute__((swift_name("init(categories:services:)"))) __attribute__((objc_designated_initializer));
- (USDKButton *)component1 __attribute__((swift_name("component1()")));
- (USDKButton *)component2 __attribute__((swift_name("component2()")));
- (USDKTabs *)doCopyCategories:(USDKButton *)categories services:(USDKButton *)services __attribute__((swift_name("doCopy(categories:services:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKButton *categories __attribute__((swift_name("categories")));
@property (readonly) USDKButton *services __attribute__((swift_name("services")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Tabs.Companion")))
@interface USDKTabsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UISettings")))
@interface USDKUISettings : USDKBase
- (instancetype)initWithButtons:(USDKButtons *)buttons firstLayer:(USDKFirstLayer *)firstLayer labels:(USDKLabels *)labels language:(USDKLanguage_ *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton secondLayer:(USDKSecondLayer *)secondLayer __attribute__((swift_name("init(buttons:firstLayer:labels:language:links:poweredBy:privacyButton:secondLayer:)"))) __attribute__((objc_designated_initializer));
- (USDKButtons *)component1 __attribute__((swift_name("component1()")));
- (USDKFirstLayer *)component2 __attribute__((swift_name("component2()")));
- (USDKLabels *)component3 __attribute__((swift_name("component3()")));
- (USDKLanguage_ *)component4 __attribute__((swift_name("component4()")));
- (USDKLinks *)component5 __attribute__((swift_name("component5()")));
- (USDKPoweredBy *)component6 __attribute__((swift_name("component6()")));
- (USDKButton *)component7 __attribute__((swift_name("component7()")));
- (USDKSecondLayer *)component8 __attribute__((swift_name("component8()")));
- (USDKUISettings *)doCopyButtons:(USDKButtons *)buttons firstLayer:(USDKFirstLayer *)firstLayer labels:(USDKLabels *)labels language:(USDKLanguage_ *)language links:(USDKLinks *)links poweredBy:(USDKPoweredBy *)poweredBy privacyButton:(USDKButton *)privacyButton secondLayer:(USDKSecondLayer *)secondLayer __attribute__((swift_name("doCopy(buttons:firstLayer:labels:language:links:poweredBy:privacyButton:secondLayer:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKButtons *buttons __attribute__((swift_name("buttons")));
@property (readonly) USDKFirstLayer *firstLayer __attribute__((swift_name("firstLayer")));
@property (readonly) USDKLabels *labels __attribute__((swift_name("labels")));
@property (readonly) USDKLanguage_ *language __attribute__((swift_name("language")));
@property (readonly) USDKLinks *links __attribute__((swift_name("links")));
@property (readonly) USDKPoweredBy *poweredBy __attribute__((swift_name("poweredBy")));
@property (readonly) USDKButton *privacyButton __attribute__((swift_name("privacyButton")));
@property (readonly) USDKSecondLayer *secondLayer __attribute__((swift_name("secondLayer")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UISettings.Companion")))
@interface USDKUISettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLs")))
@interface USDKURLs : USDKBase
- (instancetype)initWithCookiePolicy:(NSString *)cookiePolicy dataProcessingAgreement:(NSString *)dataProcessingAgreement optOut:(NSString *)optOut privacyPolicy:(NSString *)privacyPolicy __attribute__((swift_name("init(cookiePolicy:dataProcessingAgreement:optOut:privacyPolicy:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (USDKURLs *)doCopyCookiePolicy:(NSString *)cookiePolicy dataProcessingAgreement:(NSString *)dataProcessingAgreement optOut:(NSString *)optOut privacyPolicy:(NSString *)privacyPolicy __attribute__((swift_name("doCopy(cookiePolicy:dataProcessingAgreement:optOut:privacyPolicy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *cookiePolicy __attribute__((swift_name("cookiePolicy")));
@property (readonly) NSString *dataProcessingAgreement __attribute__((swift_name("dataProcessingAgreement")));
@property (readonly) NSString *optOut __attribute__((swift_name("optOut")));
@property (readonly) NSString *privacyPolicy __attribute__((swift_name("privacyPolicy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLs.Companion")))
@interface USDKURLsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLsTitle")))
@interface USDKURLsTitle : USDKBase
- (instancetype)initWithCookiePolicyTitle:(NSString *)cookiePolicyTitle dataProcessingAgreementTitle:(NSString *)dataProcessingAgreementTitle optOutTitle:(NSString *)optOutTitle privacyPolicyTitle:(NSString *)privacyPolicyTitle __attribute__((swift_name("init(cookiePolicyTitle:dataProcessingAgreementTitle:optOutTitle:privacyPolicyTitle:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (USDKURLsTitle *)doCopyCookiePolicyTitle:(NSString *)cookiePolicyTitle dataProcessingAgreementTitle:(NSString *)dataProcessingAgreementTitle optOutTitle:(NSString *)optOutTitle privacyPolicyTitle:(NSString *)privacyPolicyTitle __attribute__((swift_name("doCopy(cookiePolicyTitle:dataProcessingAgreementTitle:optOutTitle:privacyPolicyTitle:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *cookiePolicyTitle __attribute__((swift_name("cookiePolicyTitle")));
@property (readonly) NSString *dataProcessingAgreementTitle __attribute__((swift_name("dataProcessingAgreementTitle")));
@property (readonly) NSString *optOutTitle __attribute__((swift_name("optOutTitle")));
@property (readonly) NSString *privacyPolicyTitle __attribute__((swift_name("privacyPolicyTitle")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("URLsTitle.Companion")))
@interface USDKURLsTitleCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("VERSION")))
@interface USDKVERSION : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKVERSION *major __attribute__((swift_name("major")));
@property (class, readonly) USDKVERSION *minor __attribute__((swift_name("minor")));
@property (class, readonly) USDKVERSION *patch __attribute__((swift_name("patch")));
- (int32_t)compareToOther:(USDKVERSION *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeviceStorage")))
@interface USDKDeviceStorage : USDKBase
- (instancetype)initWithStorage:(USDKStorage *)storage __attribute__((swift_name("init(storage:)"))) __attribute__((objc_designated_initializer));
- (void)clear __attribute__((swift_name("clear()")));
- (void)clearStorageByKeyKey:(NSString *)key __attribute__((swift_name("clearStorageByKey(key:)")));
- (USDKStorageSettings *)fetchSettings __attribute__((swift_name("fetchSettings()")));
- (NSString *)fetchSettingsVersion __attribute__((swift_name("fetchSettingsVersion()")));
- (BOOL)fetchUserActionPerformed __attribute__((swift_name("fetchUserActionPerformed()")));
- (NSString *)getControllerId __attribute__((swift_name("getControllerId()")));
- (NSString *)getSettingsId __attribute__((swift_name("getSettingsId()")));
- (NSString *)getSettingsLanguage __attribute__((swift_name("getSettingsLanguage()")));
- (NSString * _Nullable)getValueByKeyKey:(NSString *)key __attribute__((swift_name("getValueByKey(key:)")));
- (void)doInitContext:(id)context __attribute__((swift_name("doInit(context:)")));
- (NSArray<USDKStorageService *> *)mapStorageServicesServices:(NSArray<USDKService *> *)services __attribute__((swift_name("mapStorageServices(services:)")));
- (USDKStorageSettings *)mapStorageSettingsSettings:(USDKExtendedSettings *)settings services:(NSArray<USDKService *> *)services __attribute__((swift_name("mapStorageSettings(settings:services:)")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
- (void)saveSettingsSettings:(USDKStorageSettings *)settings __attribute__((swift_name("saveSettings(settings:)")));
- (void)setUserActionPerformed __attribute__((swift_name("setUserActionPerformed()")));
- (BOOL)settingsExist __attribute__((swift_name("settingsExist()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageKeys")))
@interface USDKStorageKeys : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKStorageKeys *settings __attribute__((swift_name("settings")));
@property (class, readonly) USDKStorageKeys *userInteraction __attribute__((swift_name("userInteraction")));
- (int32_t)compareToOther:(USDKStorageKeys *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageService")))
@interface USDKStorageService : USDKBase
- (instancetype)initWithHistory:(NSArray<USDKConsentHistory *> *)history id:(NSString *)id processorId:(NSString *)processorId status:(BOOL)status __attribute__((swift_name("init(history:id:processorId:status:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKConsentHistory *> *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (BOOL)component4 __attribute__((swift_name("component4()")));
- (USDKStorageService *)doCopyHistory:(NSArray<USDKConsentHistory *> *)history id:(NSString *)id processorId:(NSString *)processorId status:(BOOL)status __attribute__((swift_name("doCopy(history:id:processorId:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKConsentHistory *> *history __attribute__((swift_name("history")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageService.Companion")))
@interface USDKStorageServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageSettings")))
@interface USDKStorageSettings : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId id:(NSString *)id language:(NSString *)language services:(NSArray<USDKStorageService *> *)services version:(NSString *)version __attribute__((swift_name("init(controllerId:id:language:services:version:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSArray<USDKStorageService *> *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (USDKStorageSettings *)doCopyControllerId:(NSString *)controllerId id:(NSString *)id language:(NSString *)language services:(NSArray<USDKStorageService *> *)services version:(NSString *)version __attribute__((swift_name("doCopy(controllerId:id:language:services:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isObjectWithValues __attribute__((swift_name("isObjectWithValues()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property (readonly) NSArray<USDKStorageService *> *services __attribute__((swift_name("services")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StorageSettings.Companion")))
@interface USDKStorageSettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EventDispatcher")))
@interface USDKEventDispatcher : USDKBase
- (instancetype)initWithEventManager:(USDKEventManager *)eventManager __attribute__((swift_name("init(eventManager:)"))) __attribute__((objc_designated_initializer));
- (void)dispatchDataTransferObject:(USDKDataTransferObject *)dataTransferObject __attribute__((swift_name("dispatch(dataTransferObject:)")));
- (void)doInitDataExchangeSettings:(NSArray<USDKDataExchangeSetting *> *)dataExchangeSettings __attribute__((swift_name("doInit(dataExchangeSettings:)")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Api")))
@interface USDKApi : USDKBase
- (instancetype)initWithDebug:(void (^)(NSString *))debug __attribute__((swift_name("init(debug:)"))) __attribute__((objc_designated_initializer));
- (void)fetchAvailableLanguagesOnSuccess:(void (^)(NSArray<NSString *> *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("fetchAvailableLanguages(onSuccess:onError:)")));
- (void)fetchServicesJsonFileNameHash:(NSString *)fileNameHash onSuccess:(void (^)(NSArray<USDKLegacyHashService *> *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("fetchServicesJson(fileNameHash:onSuccess:onError:)")));
- (void)fetchSettingsJsonOnSuccess:(void (^)(USDKLegacySettings *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("fetchSettingsJson(onSuccess:onError:)")));
- (void)fetchUserConsentsOnSuccess:(void (^)(NSArray<USDKUserConsentResponse *> *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("fetchUserConsents(onSuccess:onError:)")));
- (void)fetchUserCountryOnSuccess:(void (^)(USDKLocationData *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("fetchUserCountry(onSuccess:onError:)")));
- (void)generateServicesJsonServicesHashArray:(NSArray<USDKServiceHashArrayObject *> *)servicesHashArray onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("generateServicesJson(servicesHashArray:onSuccess:onError:)")));
- (NSString *)getControllerId __attribute__((swift_name("getControllerId()")));
- (NSString *)getJsonFileLanguage __attribute__((swift_name("getJsonFileLanguage()")));
- (NSString *)getJsonFileVersion __attribute__((swift_name("getJsonFileVersion()")));
- (NSString *)getSettingsId __attribute__((swift_name("getSettingsId()")));
- (void)resetInstance __attribute__((swift_name("resetInstance()")));
- (void)saveConsentsDataTransferObject:(USDKDataTransferObject *)dataTransferObject onSuccess:(void (^)(NSArray<USDKConsentData *> *))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("saveConsents(dataTransferObject:onSuccess:onError:)")));
- (void)setControllerIdControllerID:(NSString *)controllerID __attribute__((swift_name("setControllerId(controllerID:)")));
- (void)setJsonFileLanguageLanguage:(NSString *)language __attribute__((swift_name("setJsonFileLanguage(language:)")));
- (void)setJsonFileVersionLanguageVersion:(NSString *)languageVersion __attribute__((swift_name("setJsonFileVersion(languageVersion:)")));
- (void)setSettingsIdSettingsID:(NSString *)settingsID __attribute__((swift_name("setSettingsId(settingsID:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentData")))
@interface USDKConsentData : USDKBase
- (instancetype)initWithConsentId:(NSString *)consentId __attribute__((swift_name("init(consentId:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (USDKConsentData *)doCopyConsentId:(NSString *)consentId __attribute__((swift_name("doCopy(consentId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *consentId __attribute__((swift_name("consentId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentData.Companion")))
@interface USDKConsentDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentsList")))
@interface USDKConsentsList : USDKBase
- (instancetype)initWithData:(NSArray<USDKConsentData *> *)data __attribute__((swift_name("init(data:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKConsentData *> *)component1 __attribute__((swift_name("component1()")));
- (USDKConsentsList *)doCopyData:(NSArray<USDKConsentData *> *)data __attribute__((swift_name("doCopy(data:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKConsentData *> *data __attribute__((swift_name("data")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentsList.Companion")))
@interface USDKConsentsListCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants___")))
@interface USDKConstants___ : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (readonly) NSString *AGGREGATOR __attribute__((swift_name("AGGREGATOR")));
@property (readonly) NSString *CDN __attribute__((swift_name("CDN")));
@property (readonly) NSString *CONSENTS_GRAPHQL __attribute__((swift_name("CONSENTS_GRAPHQL")));
@property (readonly) NSString *FALLBACK_VERSION __attribute__((swift_name("FALLBACK_VERSION")));
@property (readonly) NSString *GRAPHQL __attribute__((swift_name("GRAPHQL")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Errors")))
@interface USDKErrors : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)errors __attribute__((swift_name("init()")));
@property (readonly) NSString *AVAILABLE_LANGUAGES_NOT_FOUND __attribute__((swift_name("AVAILABLE_LANGUAGES_NOT_FOUND")));
@property (readonly) NSString *FETCH_AVAILABLE_LANGUAGES __attribute__((swift_name("FETCH_AVAILABLE_LANGUAGES")));
@property (readonly) NSString *FETCH_DATA_PROCESSING_SERVICES __attribute__((swift_name("FETCH_DATA_PROCESSING_SERVICES")));
@property (readonly) NSString *FETCH_SETTINGS __attribute__((swift_name("FETCH_SETTINGS")));
@property (readonly) NSString *FETCH_USER_CONSENTS __attribute__((swift_name("FETCH_USER_CONSENTS")));
@property (readonly) NSString *FETCH_USER_COUNTRY __attribute__((swift_name("FETCH_USER_COUNTRY")));
@property (readonly) NSString *GENERATE_DATA_PROCESSING_SERVICES __attribute__((swift_name("GENERATE_DATA_PROCESSING_SERVICES")));
@property (readonly) NSString *SAVE_CONSENTS __attribute__((swift_name("SAVE_CONSENTS")));
@property (readonly) NSString *SETTINGS_NOT_FOUND __attribute__((swift_name("SETTINGS_NOT_FOUND")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetConsentsVariables")))
@interface USDKGetConsentsVariables : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId history:(BOOL)history __attribute__((swift_name("init(controllerId:history:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (USDKGetConsentsVariables *)doCopyControllerId:(NSString *)controllerId history:(BOOL)history __attribute__((swift_name("doCopy(controllerId:history:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) BOOL history __attribute__((swift_name("history")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetConsentsVariables.Companion")))
@interface USDKGetConsentsVariablesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLConsent")))
@interface USDKGraphQLConsent : USDKBase
- (instancetype)initWithAction:(NSString *)action appVersion:(NSString *)appVersion controllerId:(NSString *)controllerId consentStatus:(NSString *)consentStatus consentTemplateId:(NSString *)consentTemplateId consentTemplateVersion:(NSString *)consentTemplateVersion language:(NSString *)language processorId:(NSString *)processorId settingsId:(NSString *)settingsId settingsVersion:(NSString *)settingsVersion updatedBy:(NSString *)updatedBy __attribute__((swift_name("init(action:appVersion:controllerId:consentStatus:consentTemplateId:consentTemplateVersion:language:processorId:settingsId:settingsVersion:updatedBy:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString *)component7 __attribute__((swift_name("component7()")));
- (NSString *)component8 __attribute__((swift_name("component8()")));
- (NSString *)component9 __attribute__((swift_name("component9()")));
- (USDKGraphQLConsent *)doCopyAction:(NSString *)action appVersion:(NSString *)appVersion controllerId:(NSString *)controllerId consentStatus:(NSString *)consentStatus consentTemplateId:(NSString *)consentTemplateId consentTemplateVersion:(NSString *)consentTemplateVersion language:(NSString *)language processorId:(NSString *)processorId settingsId:(NSString *)settingsId settingsVersion:(NSString *)settingsVersion updatedBy:(NSString *)updatedBy __attribute__((swift_name("doCopy(action:appVersion:controllerId:consentStatus:consentTemplateId:consentTemplateVersion:language:processorId:settingsId:settingsVersion:updatedBy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *action __attribute__((swift_name("action")));
@property (readonly) NSString *appVersion __attribute__((swift_name("appVersion")));
@property (readonly) NSString *consentStatus __attribute__((swift_name("consentStatus")));
@property (readonly) NSString *consentTemplateId __attribute__((swift_name("consentTemplateId")));
@property (readonly) NSString *consentTemplateVersion __attribute__((swift_name("consentTemplateVersion")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) NSString *settingsId __attribute__((swift_name("settingsId")));
@property (readonly) NSString *settingsVersion __attribute__((swift_name("settingsVersion")));
@property (readonly) NSString *updatedBy __attribute__((swift_name("updatedBy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLConsent.Companion")))
@interface USDKGraphQLConsentCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQuery")))
@interface USDKGraphQLQuery : USDKBase
- (instancetype)initWithOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKGetConsentsVariables * _Nullable)variables __attribute__((swift_name("init(operationName:query:variables:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKGetConsentsVariables * _Nullable)component3 __attribute__((swift_name("component3()")));
- (USDKGraphQLQuery *)doCopyOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKGetConsentsVariables * _Nullable)variables __attribute__((swift_name("doCopy(operationName:query:variables:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *operationName __attribute__((swift_name("operationName")));
@property (readonly) NSString *query __attribute__((swift_name("query")));
@property (readonly) USDKGetConsentsVariables * _Nullable variables __attribute__((swift_name("variables")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQuery.Companion")))
@interface USDKGraphQLQueryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQueryMutation")))
@interface USDKGraphQLQueryMutation : USDKBase
- (instancetype)initWithOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKSaveConsentsVariables * _Nullable)variables __attribute__((swift_name("init(operationName:query:variables:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (USDKSaveConsentsVariables * _Nullable)component3 __attribute__((swift_name("component3()")));
- (USDKGraphQLQueryMutation *)doCopyOperationName:(NSString *)operationName query:(NSString *)query variables:(USDKSaveConsentsVariables * _Nullable)variables __attribute__((swift_name("doCopy(operationName:query:variables:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *operationName __attribute__((swift_name("operationName")));
@property (readonly) NSString *query __attribute__((swift_name("query")));
@property (readonly) USDKSaveConsentsVariables * _Nullable variables __attribute__((swift_name("variables")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLQueryMutation.Companion")))
@interface USDKGraphQLQueryMutationCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LanguageData")))
@interface USDKLanguageData : USDKBase
- (instancetype)initWithLanguagesAvailable:(NSArray<NSString *> *)languagesAvailable editableLanguages:(NSArray<NSString *> *)editableLanguages __attribute__((swift_name("init(languagesAvailable:editableLanguages:)"))) __attribute__((objc_designated_initializer));
- (NSArray<NSString *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<NSString *> *)component2 __attribute__((swift_name("component2()")));
- (USDKLanguageData *)doCopyLanguagesAvailable:(NSArray<NSString *> *)languagesAvailable editableLanguages:(NSArray<NSString *> *)editableLanguages __attribute__((swift_name("doCopy(languagesAvailable:editableLanguages:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> *editableLanguages __attribute__((swift_name("editableLanguages")));
@property (readonly) NSArray<NSString *> *languagesAvailable __attribute__((swift_name("languagesAvailable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LanguageData.Companion")))
@interface USDKLanguageDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseContainer")))
@interface USDKResponseContainer : USDKBase
- (instancetype)initWithData:(id _Nullable)data __attribute__((swift_name("init(data:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id _Nullable data __attribute__((swift_name("data")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponseContainer.Companion")))
@interface USDKResponseContainerCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializerTypeParamsSerializers:(USDKKotlinArray *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializerTypeSerial0:(id<USDKKotlinx_serialization_runtimeKSerializer>)typeSerial0 __attribute__((swift_name("serializer(typeSerial0:)")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsents")))
@interface USDKSaveConsents : USDKBase
- (instancetype)initWithSaveConsents:(USDKConsentsList *)saveConsents __attribute__((swift_name("init(saveConsents:)"))) __attribute__((objc_designated_initializer));
- (USDKConsentsList *)component1 __attribute__((swift_name("component1()")));
- (USDKSaveConsents *)doCopySaveConsents:(USDKConsentsList *)saveConsents __attribute__((swift_name("doCopy(saveConsents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKConsentsList *saveConsents __attribute__((swift_name("saveConsents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsents.Companion")))
@interface USDKSaveConsentsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsentsVariables")))
@interface USDKSaveConsentsVariables : USDKBase
- (instancetype)initWithConsents:(NSArray<USDKGraphQLConsent *> *)consents __attribute__((swift_name("init(consents:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKGraphQLConsent *> *)component1 __attribute__((swift_name("component1()")));
- (USDKSaveConsentsVariables *)doCopyConsents:(NSArray<USDKGraphQLConsent *> *)consents __attribute__((swift_name("doCopy(consents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKGraphQLConsent *> *consents __attribute__((swift_name("consents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SaveConsentsVariables.Companion")))
@interface USDKSaveConsentsVariablesCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceGenerateData")))
@interface USDKServiceGenerateData : USDKBase
- (instancetype)initWithFileName:(NSString *)fileName __attribute__((swift_name("init(fileName:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *fileName __attribute__((swift_name("fileName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServiceGenerateData.Companion")))
@interface USDKServiceGenerateDataCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServicesRequest")))
@interface USDKServicesRequest : USDKBase
- (instancetype)initWithRequest:(NSArray<USDKServiceHashArrayObject *> *)request __attribute__((swift_name("init(request:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKServiceHashArrayObject *> *)component1 __attribute__((swift_name("component1()")));
- (USDKServicesRequest *)doCopyRequest:(NSArray<USDKServiceHashArrayObject *> *)request __attribute__((swift_name("doCopy(request:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKServiceHashArrayObject *> *request __attribute__((swift_name("request")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServicesRequest.Companion")))
@interface USDKServicesRequestCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserConsentResponse")))
@interface USDKUserConsentResponse : USDKBase
- (instancetype)initWithAction:(NSString *)action settingsVersion:(NSString *)settingsVersion status:(BOOL)status templateId:(NSString *)templateId timestamp:(NSString *)timestamp updatedBy:(NSString *)updatedBy __attribute__((swift_name("init(action:settingsVersion:status:templateId:timestamp:updatedBy:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (USDKUserConsentResponse *)doCopyAction:(NSString *)action settingsVersion:(NSString *)settingsVersion status:(BOOL)status templateId:(NSString *)templateId timestamp:(NSString *)timestamp updatedBy:(NSString *)updatedBy __attribute__((swift_name("doCopy(action:settingsVersion:status:templateId:timestamp:updatedBy:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *action __attribute__((swift_name("action")));
@property (readonly) NSString *settingsVersion __attribute__((swift_name("settingsVersion")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@property (readonly) NSString *templateId __attribute__((swift_name("templateId")));
@property (readonly) NSString *timestamp __attribute__((swift_name("timestamp")));
@property (readonly) NSString *updatedBy __attribute__((swift_name("updatedBy")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserConsentResponse.Companion")))
@interface USDKUserConsentResponseCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserConsentsList")))
@interface USDKUserConsentsList : USDKBase
- (instancetype)initWithConsents:(NSArray<USDKUserConsentResponse *> *)consents __attribute__((swift_name("init(consents:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKUserConsentResponse *> *)component1 __attribute__((swift_name("component1()")));
- (USDKUserConsentsList *)doCopyConsents:(NSArray<USDKUserConsentResponse *> *)consents __attribute__((swift_name("doCopy(consents:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKUserConsentResponse *> *consents __attribute__((swift_name("consents")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserConsentsList.Companion")))
@interface USDKUserConsentsListCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CONSENT_ACTION")))
@interface USDKCONSENT_ACTION : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKCONSENT_ACTION *acceptAllServices __attribute__((swift_name("acceptAllServices")));
@property (class, readonly) USDKCONSENT_ACTION *denyAllServices __attribute__((swift_name("denyAllServices")));
@property (class, readonly) USDKCONSENT_ACTION *essentialChange __attribute__((swift_name("essentialChange")));
@property (class, readonly) USDKCONSENT_ACTION *initialPageLoad __attribute__((swift_name("initialPageLoad")));
@property (class, readonly) USDKCONSENT_ACTION *nonEuRegion __attribute__((swift_name("nonEuRegion")));
@property (class, readonly) USDKCONSENT_ACTION *sessionRestored __attribute__((swift_name("sessionRestored")));
@property (class, readonly) USDKCONSENT_ACTION *updateServices __attribute__((swift_name("updateServices")));
@property (class, readonly) USDKCONSENT_ACTION *noneAction __attribute__((swift_name("noneAction")));
- (int32_t)compareToOther:(USDKCONSENT_ACTION *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CONSENT_ACTION.Companion")))
@interface USDKCONSENT_ACTIONCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKCONSENT_ACTION *)fromS:(NSString *)s __attribute__((swift_name("from(s:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CONSENT_TYPE")))
@interface USDKCONSENT_TYPE : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKCONSENT_TYPE *explicit_ __attribute__((swift_name("explicit_")));
@property (class, readonly) USDKCONSENT_TYPE *implicit __attribute__((swift_name("implicit")));
@property (class, readonly) USDKCONSENT_TYPE *noneType __attribute__((swift_name("noneType")));
- (int32_t)compareToOther:(USDKCONSENT_TYPE *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *text __attribute__((swift_name("text")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CONSENT_TYPE.Companion")))
@interface USDKCONSENT_TYPECompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (USDKCONSENT_TYPE *)fromS:(NSString *)s __attribute__((swift_name("from(s:)")));
@end;

__attribute__((swift_name("ConsentInterface")))
@protocol USDKConsentInterface
@required
@property USDKCONSENT_ACTION *action __attribute__((swift_name("action")));
@property BOOL status __attribute__((swift_name("status")));
@property USDKCONSENT_TYPE *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent_")))
@interface USDKConsent_ : USDKBase <USDKConsentInterface>
- (instancetype)initWithAction:(USDKCONSENT_ACTION *)action status:(BOOL)status type:(USDKCONSENT_TYPE *)type __attribute__((swift_name("init(action:status:type:)"))) __attribute__((objc_designated_initializer));
- (USDKCONSENT_ACTION *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (USDKCONSENT_TYPE *)component3 __attribute__((swift_name("component3()")));
- (USDKConsent_ *)doCopyAction:(USDKCONSENT_ACTION *)action status:(BOOL)status type:(USDKCONSENT_TYPE *)type __attribute__((swift_name("doCopy(action:status:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKCONSENT_ACTION *action __attribute__((swift_name("action")));
@property BOOL status __attribute__((swift_name("status")));
@property USDKCONSENT_TYPE *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Consent_.Companion")))
@interface USDKConsent_Companion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentHistory")))
@interface USDKConsentHistory : USDKBase <USDKConsentInterface>
- (instancetype)initWithAction:(USDKCONSENT_ACTION *)action status:(BOOL)status type:(USDKCONSENT_TYPE *)type language:(NSString *)language timestamp:(double)timestamp versions:(USDKVersions *)versions __attribute__((swift_name("init(action:status:type:language:timestamp:versions:)"))) __attribute__((objc_designated_initializer));
- (USDKCONSENT_ACTION *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (USDKCONSENT_TYPE *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (double)component5 __attribute__((swift_name("component5()")));
- (USDKVersions *)component6 __attribute__((swift_name("component6()")));
- (USDKConsentHistory *)doCopyAction:(USDKCONSENT_ACTION *)action status:(BOOL)status type:(USDKCONSENT_TYPE *)type language:(NSString *)language timestamp:(double)timestamp versions:(USDKVersions *)versions __attribute__((swift_name("doCopy(action:status:type:language:timestamp:versions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property USDKCONSENT_ACTION *action __attribute__((swift_name("action")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property BOOL status __attribute__((swift_name("status")));
@property (readonly) double timestamp __attribute__((swift_name("timestamp")));
@property USDKCONSENT_TYPE *type __attribute__((swift_name("type")));
@property (readonly) USDKVersions *versions __attribute__((swift_name("versions")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConsentHistory.Companion")))
@interface USDKConsentHistoryCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataFacade")))
@interface USDKDataFacade : USDKBase
- (instancetype)initWithApi:(USDKApi *)api settings:(USDKSettingsService *)settings storage:(USDKDeviceStorage *)storage eventDispatcher:(USDKEventDispatcher *)eventDispatcher debug:(void (^)(NSString *))debug __attribute__((swift_name("init(api:settings:storage:eventDispatcher:debug:)"))) __attribute__((objc_designated_initializer));
- (void)executeServices:(NSArray<USDKService *> *)services consentAction:(USDKCONSENT_ACTION *)consentAction consentType:(USDKCONSENT_TYPE *)consentType onSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("execute(services:consentAction:consentType:onSuccess:onError:)")));
- (USDKMergedServicesSettings *)getMergedServicesAndSettingsFromStorage __attribute__((swift_name("getMergedServicesAndSettingsFromStorage()")));
- (void)mergeSettingsFromStorageCallback:(void (^)(void))callback onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("mergeSettingsFromStorage(callback:onError:)")));
- (void)restoreUserSessionOnSuccess:(void (^)(void))onSuccess onError:(void (^)(USDKKotlinThrowable *))onError __attribute__((swift_name("restoreUserSession(onSuccess:onError:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObject")))
@interface USDKDataTransferObject : USDKBase
- (instancetype)initWithApplicationVersion:(NSString *)applicationVersion consent:(USDKDataTransferObjectConsent *)consent settings:(USDKDataTransferObjectSettings *)settings services:(NSArray<USDKDataTransferObjectService *> *)services timestamp:(double)timestamp __attribute__((swift_name("init(applicationVersion:consent:settings:services:timestamp:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (USDKDataTransferObjectConsent *)component2 __attribute__((swift_name("component2()")));
- (USDKDataTransferObjectSettings *)component3 __attribute__((swift_name("component3()")));
- (NSArray<USDKDataTransferObjectService *> *)component4 __attribute__((swift_name("component4()")));
- (double)component5 __attribute__((swift_name("component5()")));
- (USDKDataTransferObject *)doCopyApplicationVersion:(NSString *)applicationVersion consent:(USDKDataTransferObjectConsent *)consent settings:(USDKDataTransferObjectSettings *)settings services:(NSArray<USDKDataTransferObjectService *> *)services timestamp:(double)timestamp __attribute__((swift_name("doCopy(applicationVersion:consent:settings:services:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *applicationVersion __attribute__((swift_name("applicationVersion")));
@property (readonly) USDKDataTransferObjectConsent *consent __attribute__((swift_name("consent")));
@property (readonly) NSArray<USDKDataTransferObjectService *> *services __attribute__((swift_name("services")));
@property (readonly) USDKDataTransferObjectSettings *settings __attribute__((swift_name("settings")));
@property (readonly) double timestamp __attribute__((swift_name("timestamp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObject.Companion")))
@interface USDKDataTransferObjectCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("DataTransferObjectConsent")))
@interface USDKDataTransferObjectConsent : USDKBase
- (instancetype)initWithAction:(USDKCONSENT_ACTION *)action type:(USDKCONSENT_TYPE *)type __attribute__((swift_name("init(action:type:)"))) __attribute__((objc_designated_initializer));
@property (readonly) USDKCONSENT_ACTION *action __attribute__((swift_name("action")));
@property (readonly) USDKCONSENT_TYPE *type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectConsent.Companion")))
@interface USDKDataTransferObjectConsentCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectService")))
@interface USDKDataTransferObjectService : USDKBase
- (instancetype)initWithId:(NSString *)id name:(NSString *)name status:(BOOL)status version:(NSString *)version processorId:(NSString *)processorId __attribute__((swift_name("init(id:name:status:version:processorId:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (USDKDataTransferObjectService *)doCopyId:(NSString *)id name:(NSString *)name status:(BOOL)status version:(NSString *)version processorId:(NSString *)processorId __attribute__((swift_name("doCopy(id:name:status:version:processorId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *processorId __attribute__((swift_name("processorId")));
@property (readonly) BOOL status __attribute__((swift_name("status")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectService.Companion")))
@interface USDKDataTransferObjectServiceCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectSettings")))
@interface USDKDataTransferObjectSettings : USDKBase
- (instancetype)initWithControllerId:(NSString *)controllerId id:(NSString *)id language:(NSString *)language version:(NSString *)version __attribute__((swift_name("init(controllerId:id:language:version:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (USDKDataTransferObjectSettings *)doCopyControllerId:(NSString *)controllerId id:(NSString *)id language:(NSString *)language version:(NSString *)version __attribute__((swift_name("doCopy(controllerId:id:language:version:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *controllerId __attribute__((swift_name("controllerId")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString *language __attribute__((swift_name("language")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataTransferObjectSettings.Companion")))
@interface USDKDataTransferObjectSettingsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EssentialServices")))
@interface USDKEssentialServices : USDKBase
- (instancetype)initWithMergedEssentialServices:(NSArray<USDKService *> *)mergedEssentialServices updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("init(mergedEssentialServices:updatedEssentialServices:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKService *> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<USDKService *> *)component2 __attribute__((swift_name("component2()")));
- (USDKEssentialServices *)doCopyMergedEssentialServices:(NSArray<USDKService *> *)mergedEssentialServices updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("doCopy(mergedEssentialServices:updatedEssentialServices:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKService *> *mergedEssentialServices __attribute__((swift_name("mergedEssentialServices")));
@property (readonly) NSArray<USDKService *> *updatedEssentialServices __attribute__((swift_name("updatedEssentialServices")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MergedServicesSettings")))
@interface USDKMergedServicesSettings : USDKBase
- (instancetype)initWithMergedServices:(NSArray<USDKService *> *)mergedServices mergedSettings:(USDKExtendedSettings *)mergedSettings updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("init(mergedServices:mergedSettings:updatedEssentialServices:)"))) __attribute__((objc_designated_initializer));
- (NSArray<USDKService *> *)component1 __attribute__((swift_name("component1()")));
- (USDKExtendedSettings *)component2 __attribute__((swift_name("component2()")));
- (NSArray<USDKService *> *)component3 __attribute__((swift_name("component3()")));
- (USDKMergedServicesSettings *)doCopyMergedServices:(NSArray<USDKService *> *)mergedServices mergedSettings:(USDKExtendedSettings *)mergedSettings updatedEssentialServices:(NSArray<USDKService *> *)updatedEssentialServices __attribute__((swift_name("doCopy(mergedServices:mergedSettings:updatedEssentialServices:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<USDKService *> *mergedServices __attribute__((swift_name("mergedServices")));
@property (readonly) USDKExtendedSettings *mergedSettings __attribute__((swift_name("mergedSettings")));
@property (readonly) NSArray<USDKService *> *updatedEssentialServices __attribute__((swift_name("updatedEssentialServices")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Versions")))
@interface USDKVersions : USDKBase
- (instancetype)initWithApplication:(NSString *)application service:(NSString *)service settings:(NSString *)settings __attribute__((swift_name("init(application:service:settings:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (USDKVersions *)doCopyApplication:(NSString *)application service:(NSString *)service settings:(NSString *)settings __attribute__((swift_name("doCopy(application:service:settings:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *application __attribute__((swift_name("application")));
@property (readonly) NSString *service __attribute__((swift_name("service")));
@property (readonly) NSString *settings __attribute__((swift_name("settings")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Versions.Companion")))
@interface USDKVersionsCompanion : USDKBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApplicationDispatcherKt")))
@interface USDKApplicationDispatcherKt : USDKBase
@property (class, readonly) USDKKotlinx_coroutines_coreCoroutineDispatcher *ApplicationDispatcher __attribute__((swift_name("ApplicationDispatcher")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsercentricsKt")))
@interface USDKUsercentricsKt : USDKBase
@property (class, readonly) NSString *NOT_INITIALIZED_ERROR __attribute__((swift_name("NOT_INITIALIZED_ERROR")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActualKt")))
@interface USDKActualKt : USDKBase
+ (id)generateUsercentricsViewContext:(id)context sharedVariable:(NSString *)sharedVariable dismissView:(void (^)(void))dismissView __attribute__((swift_name("generateUsercentricsView(context:sharedVariable:dismissView:)")));
+ (NSString *)platformLanguage __attribute__((swift_name("platformLanguage()")));
+ (UIView *)shadowUiViewCornerRadius:(USDKDouble * _Nullable)cornerRadius shadowRadius:(USDKDouble * _Nullable)shadowRadius shadowOffsetX:(double)shadowOffsetX shadowOffsetY:(double)shadowOffsetY shadowColorAlpha:(double)shadowColorAlpha shadowColor:(UIColor *)shadowColor setup:(void (^)(UIView *))setup __attribute__((swift_name("shadowUiView(cornerRadius:shadowRadius:shadowOffsetX:shadowOffsetY:shadowColorAlpha:shadowColor:setup:)")));
+ (void)addDropShadow:(UIView *)receiver radius:(double)radius offsetX:(double)offsetX offsetY:(double)offsetY color:(UIColor *)color __attribute__((swift_name("addDropShadow(_:radius:offsetX:offsetY:color:)")));
+ (void)roundCorners:(UIView *)receiver radius:(double)radius shouldRoundTop:(BOOL)shouldRoundTop shouldRoundRight:(BOOL)shouldRoundRight shouldRoundBottom:(BOOL)shouldRoundBottom shouldRoundLeft:(BOOL)shouldRoundLeft shouldRoundTopLeft:(BOOL)shouldRoundTopLeft shouldRoundTopRight:(BOOL)shouldRoundTopRight shouldRoundBottomLeft:(BOOL)shouldRoundBottomLeft shouldRoundBottomRight:(BOOL)shouldRoundBottomRight __attribute__((swift_name("roundCorners(_:radius:shouldRoundTop:shouldRoundRight:shouldRoundBottom:shouldRoundLeft:shouldRoundTopLeft:shouldRoundTopRight:shouldRoundBottomLeft:shouldRoundBottomRight:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommonKt")))
@interface USDKCommonKt : USDKBase
+ (USDKResourcesImageResource *)getLogo __attribute__((swift_name("getLogo()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HelpersKt")))
@interface USDKHelpersKt : USDKBase
+ (NSString *)getApplicationVersion __attribute__((swift_name("getApplicationVersion()")));
+ (NSString *)hashFunctionInput:(NSString *)input __attribute__((swift_name("hashFunction(input:)")));
@property (class, readonly) NSString *applicationVersion __attribute__((swift_name("applicationVersion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MapKt")))
@interface USDKMapKt : USDKBase
+ (NSString *)createServicesJsonHashServicesHashArray:(NSArray<USDKServiceHashArrayObject *> *)servicesHashArray __attribute__((swift_name("createServicesJsonHash(servicesHashArray:)")));
+ (USDKLegacyHashService *)findServicesFromHashArrayLegacyService:(id<USDKLegacyBaseServiceInterface>)legacyService legacyHashServices:(NSArray<USDKLegacyHashService *> *)legacyHashServices __attribute__((swift_name("findServicesFromHashArray(legacyService:legacyHashServices:)")));
+ (NSString *)generateConsentIdSettingsId:(NSString *)settingsId controllerId:(NSString *)controllerId processorId:(NSString *)processorId __attribute__((swift_name("generateConsentId(settingsId:controllerId:processorId:)")));
+ (NSString *)generateIdentityId __attribute__((swift_name("generateIdentityId()")));
+ (USDKBaseService *)mapBaseServiceLegacyService:(id<USDKLegacyBaseServiceInterface>)legacyService legacyHashServices:(NSArray<USDKLegacyHashService *> *)legacyHashServices __attribute__((swift_name("mapBaseService(legacyService:legacyHashServices:)")));
+ (NSArray<USDKCategory *> *)mapCategoriesLegacySettings:(USDKLegacySettings *)legacySettings legacyHashServices:(NSArray<USDKLegacyHashService *> *)legacyHashServices __attribute__((swift_name("mapCategories(legacySettings:legacyHashServices:)")));
+ (NSArray<USDKDataExchangeSetting *> *)mapDataExchangeSettingsLegacyDataExchangeSettings:(NSArray<USDKLegacyDataExchangeSetting *> *)legacyDataExchangeSettings __attribute__((swift_name("mapDataExchangeSettings(legacyDataExchangeSettings:)")));
+ (USDKServiceHashArrayObject *)mapHashArrayObjectLegacyService:(id<USDKLegacyBaseServiceInterface>)legacyService legacySettings:(USDKLegacySettings *)legacySettings __attribute__((swift_name("mapHashArrayObject(legacyService:legacySettings:)")));
+ (USDKService *)mapServiceLegacyService:(USDKLegacyService *)legacyService legacyHashServices:(NSArray<USDKLegacyHashService *> *)legacyHashServices legacyCategory:(USDKLegacyCategory *)legacyCategory __attribute__((swift_name("mapService(legacyService:legacyHashServices:legacyCategory:)")));
+ (NSArray<USDKService *> *)mapServicesLegacySettings:(USDKLegacySettings *)legacySettings legacyHashServices:(NSArray<USDKLegacyHashService *> *)legacyHashServices legacyCategory:(USDKLegacyCategory *)legacyCategory __attribute__((swift_name("mapServices(legacySettings:legacyHashServices:legacyCategory:)")));
+ (NSArray<USDKServiceHashArrayObject *> *)mapServicesHashArrayLegacySettings:(USDKLegacySettings *)legacySettings __attribute__((swift_name("mapServicesHashArray(legacySettings:)")));
+ (USDKExtendedSettings *)mapSettingsLegacySettings:(USDKLegacySettings *)legacySettings legacyHashServices:(NSArray<USDKLegacyHashService *> *)legacyHashServices controllerId:(NSString * _Nullable)controllerId __attribute__((swift_name("mapSettings(legacySettings:legacyHashServices:controllerId:)")));
+ (NSArray<USDKInt *> *)mapShowFirstLayerOnVersionChangeShowInitialViewForVersionChange:(NSArray<NSString *> *)showInitialViewForVersionChange __attribute__((swift_name("mapShowFirstLayerOnVersionChange(showInitialViewForVersionChange:)")));
+ (NSArray<USDKBaseService *> *)mapSubServicesLegacySubServices:(NSArray<USDKLegacyBaseService *> *)legacySubServices legacyHashServices:(NSArray<USDKLegacyHashService *> *)legacyHashServices __attribute__((swift_name("mapSubServices(legacySubServices:legacyHashServices:)")));
+ (USDKUISettings * _Nullable)mapUISettingsLegacySettings:(USDKLegacySettings *)legacySettings __attribute__((swift_name("mapUISettings(legacySettings:)")));
+ (USDKLegacySettings *)removeDeactivatedServicesLegacySettings:(USDKLegacySettings *)legacySettings __attribute__((swift_name("removeDeactivatedServices(legacySettings:)")));
+ (NSString *)resolveDataProcessorLegacyHashService:(USDKLegacyHashService *)legacyHashService __attribute__((swift_name("resolveDataProcessor(legacyHashService:)")));
+ (NSArray<NSString *> *)resolveDataPurposesListLegacyHashService:(USDKLegacyHashService *)legacyHashService __attribute__((swift_name("resolveDataPurposesList(legacyHashService:)")));
+ (NSString *)resolveDescriptionLegacyService:(id<USDKLegacyBaseServiceInterface>)legacyService legacyHashService:(USDKLegacyHashService *)legacyHashService __attribute__((swift_name("resolveDescription(legacyService:legacyHashService:)")));
+ (BOOL)resolveIsHiddenLegacyService:(USDKLegacyService *)legacyService legacyCategory:(USDKLegacyCategory *)legacyCategory __attribute__((swift_name("resolveIsHidden(legacyService:legacyCategory:)")));
+ (NSArray<NSString *> *)resolveLegalBasisListLegacyHashService:(USDKLegacyHashService *)legacyHashService __attribute__((swift_name("resolveLegalBasisList(legacyHashService:)")));
+ (NSString *)resolvePrivacyPolicyUrlLegacyHashService:(USDKLegacyHashService *)legacyHashService __attribute__((swift_name("resolvePrivacyPolicyUrl(legacyHashService:)")));
+ (NSString *)resolveProcessingCompanyNameLegacyHashService:(USDKLegacyHashService *)legacyHashService __attribute__((swift_name("resolveProcessingCompanyName(legacyHashService:)")));
+ (NSString *)resolveRetentionPeriodDescriptionLegacyHashService:(USDKLegacyHashService *)legacyHashService __attribute__((swift_name("resolveRetentionPeriodDescription(legacyHashService:)")));
+ (BOOL)resolveStatusLegacyService:(USDKLegacyService *)legacyService legacyCategory:(USDKLegacyCategory *)legacyCategory __attribute__((swift_name("resolveStatus(legacyService:legacyCategory:)")));
+ (NSArray<NSString *> *)resolveStringToListLegacyHashService:(USDKLegacyHashService *)legacyHashService property:(USDKLEGACY_STRING_TO_LIST_PROPERTIES *)property __attribute__((swift_name("resolveStringToList(legacyHashService:property:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConstantsKt")))
@interface USDKConstantsKt : USDKBase
@property (class, readonly) NSString *CONSENT_STATUS_EVENT __attribute__((swift_name("CONSENT_STATUS_EVENT")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResourcesImageResource")))
@interface USDKResourcesImageResource : USDKBase
- (instancetype)initWithAssetImageName:(NSString *)assetImageName bundle:(NSBundle *)bundle __attribute__((swift_name("init(assetImageName:bundle:)"))) __attribute__((objc_designated_initializer));
- (UIImage * _Nullable)toUIImage __attribute__((swift_name("toUIImage()")));
@property (readonly) NSString *assetImageName __attribute__((swift_name("assetImageName")));
@property (readonly) NSBundle *bundle __attribute__((swift_name("bundle")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface USDKKotlinThrowable : USDKBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(USDKKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (USDKKotlinArray *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) USDKKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((swift_name("Ktor_utilsStringValuesBuilder")))
@interface USDKKtor_utilsStringValuesBuilder : USDKBase
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer));
- (void)appendName:(NSString *)name value:(NSString *)value __attribute__((swift_name("append(name:value:)")));
- (void)appendAllStringValues:(id<USDKKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendAll(stringValues:)")));
- (void)appendAllName:(NSString *)name values:(id)values __attribute__((swift_name("appendAll(name:values:)")));
- (void)appendMissingStringValues:(id<USDKKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendMissing(stringValues:)")));
- (void)appendMissingName:(NSString *)name values:(id)values __attribute__((swift_name("appendMissing(name:values:)")));
- (id<USDKKtor_utilsStringValues>)build __attribute__((swift_name("build()")));
- (void)clear __attribute__((swift_name("clear()")));
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<USDKKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
- (void)removeName:(NSString *)name __attribute__((swift_name("remove(name:)")));
- (BOOL)removeName:(NSString *)name value:(NSString *)value __attribute__((swift_name("remove(name:value:)")));
- (void)removeKeysWithNoEntries __attribute__((swift_name("removeKeysWithNoEntries()")));
- (void)setName:(NSString *)name value:(NSString *)value __attribute__((swift_name("set(name:value:)")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@property BOOL built __attribute__((swift_name("built")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@property (readonly) USDKMutableDictionary<NSString *, NSMutableArray<NSString *> *> *values __attribute__((swift_name("values")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeadersBuilder")))
@interface USDKKtor_httpHeadersBuilder : USDKKtor_utilsStringValuesBuilder
- (instancetype)initWithSize:(int32_t)size __attribute__((swift_name("init(size:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<USDKKtor_httpHeaders>)build __attribute__((swift_name("build()")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface USDKKotlinArray : USDKBase
+ (instancetype)arrayWithSize:(int32_t)size init:(id _Nullable (^)(USDKInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (id _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<USDKKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(id _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeEncoder")))
@protocol USDKKotlinx_serialization_runtimeEncoder
@required
- (id<USDKKotlinx_serialization_runtimeCompositeEncoder>)beginCollectionDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize typeSerializers:(USDKKotlinArray *)typeSerializers __attribute__((swift_name("beginCollection(descriptor:collectionSize:typeSerializers:)")));
- (id<USDKKotlinx_serialization_runtimeCompositeEncoder>)beginStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor typeSerializers:(USDKKotlinArray *)typeSerializers __attribute__((swift_name("beginStructure(descriptor:typeSerializers:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
- (void)encodeUnit __attribute__((swift_name("encodeUnit()")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialDescriptor")))
@protocol USDKKotlinx_serialization_runtimeSerialDescriptor
@required
- (NSArray<id<USDKKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<USDKKotlinx_serialization_runtimeSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (NSArray<id<USDKKotlinAnnotation>> *)getEntityAnnotations __attribute__((swift_name("getEntityAnnotations()"))) __attribute__((deprecated("Deprecated in the favour of 'annotations' property")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<USDKKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) USDKKotlinx_serialization_runtimeSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *name __attribute__((swift_name("name"))) __attribute__((unavailable("name property deprecated in the favour of serialName")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeDecoder")))
@protocol USDKKotlinx_serialization_runtimeDecoder
@required
- (id<USDKKotlinx_serialization_runtimeCompositeDecoder>)beginStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor typeParams:(USDKKotlinArray *)typeParams __attribute__((swift_name("beginStructure(descriptor:typeParams:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (USDKKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
- (void)decodeUnit __attribute__((swift_name("decodeUnit()")));
- (id _Nullable)updateNullableSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableValue(deserializer:old:)")));
- (id _Nullable)updateSerializableValueDeserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableValue(deserializer:old:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) USDKKotlinx_serialization_runtimeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeJsonElement")))
@interface USDKKotlinx_serialization_runtimeJsonElement : USDKBase
- (BOOL)containsKey:(NSString *)key __attribute__((swift_name("contains(key:)")));
@property (readonly) BOOL isNull __attribute__((swift_name("isNull")));
@property (readonly) NSArray<USDKKotlinx_serialization_runtimeJsonElement *> *jsonArray __attribute__((swift_name("jsonArray")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonNull *jsonNull __attribute__((swift_name("jsonNull")));
@property (readonly) NSDictionary<NSString *, USDKKotlinx_serialization_runtimeJsonElement *> *jsonObject __attribute__((swift_name("jsonObject")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonPrimitive *primitive __attribute__((swift_name("primitive")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol USDKKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<USDKKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<USDKKotlinCoroutineContextElement> _Nullable)getKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<USDKKotlinCoroutineContext>)minusKeyKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<USDKKotlinCoroutineContext>)plusContext:(id<USDKKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol USDKKotlinCoroutineContextElement <USDKKotlinCoroutineContext>
@required
@property (readonly) id<USDKKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextElement")))
@interface USDKKotlinAbstractCoroutineContextElement : USDKBase <USDKKotlinCoroutineContextElement>
- (instancetype)initWithKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<USDKKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinContinuationInterceptor")))
@protocol USDKKotlinContinuationInterceptor <USDKKotlinCoroutineContextElement>
@required
- (id<USDKKotlinContinuation>)interceptContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (void)releaseInterceptedContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher")))
@interface USDKKotlinx_coroutines_coreCoroutineDispatcher : USDKKotlinAbstractCoroutineContextElement <USDKKotlinContinuationInterceptor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithKey:(id<USDKKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)dispatchContext:(id<USDKKotlinCoroutineContext>)context block:(id<USDKKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatch(context:block:)")));
- (void)dispatchYieldContext:(id<USDKKotlinCoroutineContext>)context block:(id<USDKKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatchYield(context:block:)")));
- (id<USDKKotlinContinuation>)interceptContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (BOOL)isDispatchNeededContext:(id<USDKKotlinCoroutineContext>)context __attribute__((swift_name("isDispatchNeeded(context:)")));
- (USDKKotlinx_coroutines_coreCoroutineDispatcher *)plusOther:(USDKKotlinx_coroutines_coreCoroutineDispatcher *)other __attribute__((swift_name("plus(other:)"))) __attribute__((unavailable("Operator '+' on two CoroutineDispatcher objects is meaningless. CoroutineDispatcher is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The dispatcher to the right of `+` just replaces the dispatcher to the left.")));
- (void)releaseInterceptedContinuationContinuation:(id<USDKKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_utilsStringValues")))
@protocol USDKKtor_utilsStringValues
@required
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<USDKKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (void)forEachBody:(void (^)(NSString *, NSArray<NSString *> *))body __attribute__((swift_name("forEach(body:)")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@end;

__attribute__((swift_name("KotlinMapEntry")))
@protocol USDKKotlinMapEntry
@required
@property (readonly) id _Nullable key __attribute__((swift_name("key")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Ktor_httpHeaders")))
@protocol USDKKtor_httpHeaders <USDKKtor_utilsStringValues>
@required
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol USDKKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeCompositeEncoder")))
@protocol USDKKotlinx_serialization_runtimeCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (void)encodeIntElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNonSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(id)value __attribute__((swift_name("encodeNonSerializableElement(descriptor:index:value:)"))) __attribute__((unavailable("This method is deprecated for removal. Please remove it from your implementation and delegate to default method instead")));
- (void)encodeNullableSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<USDKKotlinx_serialization_runtimeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)encodeUnitElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeUnitElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialModule")))
@protocol USDKKotlinx_serialization_runtimeSerialModule
@required
- (void)dumpToCollector:(id<USDKKotlinx_serialization_runtimeSerialModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer> _Nullable)getContextualKclass:(id<USDKKotlinKClass>)kclass __attribute__((swift_name("getContextual(kclass:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer> _Nullable)getPolymorphicBaseClass:(id<USDKKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<USDKKotlinx_serialization_runtimeKSerializer> _Nullable)getPolymorphicBaseClass:(id<USDKKotlinKClass>)baseClass serializedClassName:(NSString *)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol USDKKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialKind")))
@interface USDKKotlinx_serialization_runtimeSerialKind : USDKBase
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeCompositeDecoder")))
@protocol USDKKotlinx_serialization_runtimeCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:)")));
- (int16_t)decodeShortElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)decodeUnitElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeUnitElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (id _Nullable)updateNullableSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableElement(descriptor:index:deserializer:old:)")));
- (id _Nullable)updateSerializableElementDescriptor:(id<USDKKotlinx_serialization_runtimeSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<USDKKotlinx_serialization_runtimeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableElement(descriptor:index:deserializer:old:)")));
@property (readonly) id<USDKKotlinx_serialization_runtimeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) USDKKotlinx_serialization_runtimeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface USDKKotlinNothing : USDKBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeUpdateMode")))
@interface USDKKotlinx_serialization_runtimeUpdateMode : USDKKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) USDKKotlinx_serialization_runtimeUpdateMode *banned __attribute__((swift_name("banned")));
@property (class, readonly) USDKKotlinx_serialization_runtimeUpdateMode *overwrite __attribute__((swift_name("overwrite")));
@property (class, readonly) USDKKotlinx_serialization_runtimeUpdateMode *update __attribute__((swift_name("update")));
- (int32_t)compareToOther:(USDKKotlinx_serialization_runtimeUpdateMode *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeJsonPrimitive")))
@interface USDKKotlinx_serialization_runtimeJsonPrimitive : USDKKotlinx_serialization_runtimeJsonElement
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL boolean __attribute__((swift_name("boolean")));
@property (readonly) USDKBoolean * _Nullable booleanOrNull __attribute__((swift_name("booleanOrNull")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSString * _Nullable contentOrNull __attribute__((swift_name("contentOrNull")));
@property (readonly, getter=double) double double_ __attribute__((swift_name("double_")));
@property (readonly) USDKDouble * _Nullable doubleOrNull __attribute__((swift_name("doubleOrNull")));
@property (readonly, getter=float) float float_ __attribute__((swift_name("float_")));
@property (readonly) USDKFloat * _Nullable floatOrNull __attribute__((swift_name("floatOrNull")));
@property (readonly, getter=int) int32_t int_ __attribute__((swift_name("int_")));
@property (readonly) USDKInt * _Nullable intOrNull __attribute__((swift_name("intOrNull")));
@property (readonly, getter=long) int64_t long_ __attribute__((swift_name("long_")));
@property (readonly) USDKLong * _Nullable longOrNull __attribute__((swift_name("longOrNull")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonPrimitive *primitive __attribute__((swift_name("primitive")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtimeJsonNull")))
@interface USDKKotlinx_serialization_runtimeJsonNull : USDKKotlinx_serialization_runtimeJsonPrimitive
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)jsonNull __attribute__((swift_name("init()")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSString * _Nullable contentOrNull __attribute__((swift_name("contentOrNull")));
@property (readonly) USDKKotlinx_serialization_runtimeJsonNull *jsonNull __attribute__((swift_name("jsonNull")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol USDKKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol USDKKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<USDKKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreRunnable")))
@protocol USDKKotlinx_coroutines_coreRunnable
@required
- (void)run __attribute__((swift_name("run()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtimeSerialModuleCollector")))
@protocol USDKKotlinx_serialization_runtimeSerialModuleCollector
@required
- (void)contextualKClass:(id<USDKKotlinKClass>)kClass serializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<USDKKotlinKClass>)baseClass actualClass:(id<USDKKotlinKClass>)actualClass actualSerializer:(id<USDKKotlinx_serialization_runtimeKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol USDKKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol USDKKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol USDKKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol USDKKotlinKClass <USDKKotlinKDeclarationContainer, USDKKotlinKAnnotatedElement, USDKKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END

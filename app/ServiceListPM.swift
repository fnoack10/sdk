//
//  ServiceListPM.swift
//  UserCentrics
//
//  Created by Franco Noack on 07.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import Foundation
import UsercentricsSDK

struct ServiceListPM {
    var list: [ServicePM]
    
    init(_ services: [Service], categories: [UsercentricsSDK.Category] = [], settings: Settings? = nil) {
        self.list = services.filter { !$0.isHidden }.compactMap { ServicePM($0, categories: categories, settings: settings) }
    }
    
    func servicesForCategory(_ slug: String) -> [ServicePM] {
        return self.list.filter { $0.categorySlug == slug }
    }
    
    mutating func setConsentForCategory(_ isConsented: Bool, categorySlug: String) {
        var updatedServices: [ServicePM] = []
        for var service in list {
            if service.categorySlug == categorySlug {
                service.isConsented = isConsented
                updatedServices.append(service)
            } else {
                updatedServices.append(service)
            }
        }
        self.list = updatedServices
    }
}

struct ServicePM {
    let id: String
    let name: String
    let description: String
    let categoryName: String
    let categorySlug: String
    let isEssencial: Bool
    let isHidden: Bool
    var details: [ServiceDetailPM] = []
    
    
    var isSelected: Bool = false
    var isConsented: Bool
    
    
    init(_ service: Service, categories: [UsercentricsSDK.Category], settings: Settings?) {
        self.id = service.id
        self.name = service.name
        self.description = service.description()
        self.categoryName = categories.filter { $0.slug == service.categorySlug }.first?.label ?? ""
        self.categorySlug = service.categorySlug
        self.isEssencial = service.isEssential
        self.isHidden = service.isHidden
        self.isConsented = service.consent.status
        
        guard let settings = settings else { return }
        let generalLabels = settings.ui.labels.general
        let labels = settings.ui.labels.service
        
        ServiceDetailType.allCases.forEach { detail in
            var title: String = ""
            var description: String = ""
            switch detail {
            case .serviceDescription:
                title = labels.descriptionTitle
                description = service.description()
            case .processingCompany:
                title = labels.processingCompanyTitle
                if !service.processingCompany.name.isEmpty && !service.processingCompany.address.isEmpty {
                    description = service.processingCompany.name + "\n" + service.processingCompany.address
                }
            case .dataPurposes:
                title = labels.dataPurposes.title
                description = service.dataPurposes.description
            case .technologiesUsed:
                title = labels.technologiesUsed.title
                description = service.technologiesUsed.description
            case .dataCollected:
                title = labels.dataCollected.title
                description = service.dataCollected.description
            case .legalBasis:
                title = labels.legalBasis.title
                description = service.legalBasis.description
            case .processingLocation:
                title = labels.dataDistribution.processingLocationTitle
                description = service.dataDistribution.processingLocation
            case .rentantionPeriod:
                title = labels.retentionPeriodTitle
                description = service.retentionPeriodDescription
            case .thirdCountyDistribution:
                title = labels.dataDistribution.thirdPartyCountriesTitle
                description = service.dataDistribution.thirdPartyCountries
            case .dataRecipients:
                title = labels.dataRecipientsTitle
                if !service.processingCompany.dataProtectionOfficer.isEmpty && !service.processingCompany.name.isEmpty && !service.processingCompany.address.isEmpty {
                    description = service.processingCompany.dataProtectionOfficer + "\n" + service.processingCompany.name + "\n" + service.processingCompany.address
                }
            case .privacyPolicy:
                title = labels.urls.privacyPolicyTitle
                description = service.urls.privacyPolicy
            case .cookiePolicy:
                title = labels.urls.cookiePolicyTitle
                description = service.urls.cookiePolicy
            case .optOutLink:
                title = labels.urls.optOutTitle
                description = service.urls.optOut
            case .history:
                title = labels.history.title
                description = service.consent.history.description
            case .controllerID:
                title = generalLabels.controllerId
                description = settings.controllerId
            case .processorID:
                title = generalLabels.processorId
                description = service.processorId
            }
            
            if !description.isEmpty {
                let detail = ServiceDetailPM(title: title, description: description)
                details.append(detail)
            }
        }
    }
}

enum ServiceDetailType: CaseIterable {
    case serviceDescription
    case processingCompany
    case dataPurposes
    case technologiesUsed
    case dataCollected
    case legalBasis
    case processingLocation
    case rentantionPeriod
    case thirdCountyDistribution
    case dataRecipients
    case privacyPolicy
    case cookiePolicy
    case optOutLink
    case history
    case controllerID
    case processorID
}

struct ServiceDetailPM {
    let title: String
    let description: String
    
    init(title: String, description: String) {
        self.title = title
        self.description = description
    }
}

struct CategoryListPM {
    var list: [CategoryPM]
    
    init(_ categories: [UsercentricsSDK.Category], services: ServiceListPM) {
        self.list = categories.compactMap { category in
            let categoryServices = services.servicesForCategory(category.slug)
            return CategoryPM(category, services: categoryServices) }
    }
}


//
//  Theme.swift
//  UserCentrics
//
//  Created by Franco Noack on 05.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import UIKit

class Theme {
    var topIcon: UIImage
    var screenshot: UIImage
    var mainColor: UIColor
    var backgroundColor: UIColor
    var textColor: UIColor
    var accessType: AccessType
    var primaryButtonBackgroundColor: UIColor
    var secondaryButtonBackgroundColor: UIColor
    var tertiaryButtonBackgroundColor: UIColor
    var primaryButtonTextColor: UIColor
    var secondaryButtonTextColor: UIColor
    var tertiaryButtonTextColor: UIColor
    
    init(topIcon: UIImage,
         screenshot: UIImage,
         mainColor: UIColor,
         backgroundColor: UIColor = UIColor.white,
         textColor: UIColor = UIColor.init(hex: 0x303030),
         primaryButtonBackgroundColor: UIColor = UIColor.init(hex: 0xF5F5F5),
         secondaryButtonBackgroundColor: UIColor = UIColor.init(hex: 0xF5F5F5),
         tertiaryButtonBackgroundColor: UIColor = UIColor.init(hex: 0xF5F5F5),
         primaryButtonTextColor: UIColor = UIColor.init(hex: 0x303030),
         secondaryButtonTextColor: UIColor = UIColor.init(hex: 0x303030),
         tertiaryButtonTextColor: UIColor = UIColor.init(hex: 0x303030),
         accessType: AccessType){
        
        self.topIcon = topIcon
        self.screenshot = screenshot
        self.mainColor = mainColor
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.primaryButtonBackgroundColor = primaryButtonBackgroundColor
        self.secondaryButtonBackgroundColor = secondaryButtonBackgroundColor
        self.tertiaryButtonBackgroundColor = tertiaryButtonBackgroundColor
        self.primaryButtonTextColor = primaryButtonTextColor
        self.secondaryButtonTextColor = secondaryButtonTextColor
        self.tertiaryButtonTextColor = tertiaryButtonTextColor
        self.accessType = accessType
    }
}

extension Theme {
    
    static var usercentrics: Theme {
        let topIcon = UIImage(named: "usercentrics") ?? UIImage()
        let screenshot = UIImage(named: "usercentrics_screenshot") ?? UIImage()
        let mainColor = UIColor.init(hex: 0x0045A5)
        let primaryButtonBackgroundColor = mainColor
        let primaryButtonTextColor = UIColor.white
        let accessType = AccessType.mainButton
        return Theme(topIcon: topIcon,
                     screenshot: screenshot,
                     mainColor: mainColor,
                     primaryButtonBackgroundColor: primaryButtonBackgroundColor,
                     primaryButtonTextColor: primaryButtonTextColor,
                     accessType: accessType)
    }
    
    static var zalando: Theme {
        let topIcon = UIImage(named: "zalando") ?? UIImage()
        let screenshot = UIImage(named: "zalando_screenshot") ?? UIImage()
        let mainColor = UIColor(red: 255/255.0, green: 105/255.0, blue: 17/255.0, alpha: 1)
        let primaryButtonBackgroundColor = mainColor
        let secondaryButtonBackgroundColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1)
        let tertiaryButtonBackgroundColor = secondaryButtonBackgroundColor
        let primaryButtonTextColor = UIColor.white
        let accessType = AccessType.privacyBubble
        return Theme(topIcon: topIcon,
                     screenshot: screenshot,
                     mainColor: mainColor,
                     primaryButtonBackgroundColor: primaryButtonBackgroundColor,
                     secondaryButtonBackgroundColor: secondaryButtonBackgroundColor,
                     tertiaryButtonBackgroundColor: tertiaryButtonBackgroundColor,
                     primaryButtonTextColor: primaryButtonTextColor,
                     accessType: accessType)
    }
    
    static var commerzbank: Theme {
        let topIcon = UIImage(named: "commerzbank") ?? UIImage()
        let screenshot = UIImage(named: "commerzbank_screenshot") ?? UIImage()
        let mainColor = UIColor(red: 255/255.0, green: 204/255.0, blue: 67/255.0, alpha: 1)
        let primaryButtonBackgroundColor = mainColor
        let secondaryButtonBackgroundColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1)
        let tertiaryButtonBackgroundColor = secondaryButtonBackgroundColor
        let accessType = AccessType.privacyBubble
        return Theme(topIcon: topIcon,
                     screenshot: screenshot,
                     mainColor: mainColor,
                     primaryButtonBackgroundColor: primaryButtonBackgroundColor,
                     secondaryButtonBackgroundColor: secondaryButtonBackgroundColor,
                     tertiaryButtonBackgroundColor: tertiaryButtonBackgroundColor,
                     accessType: accessType)
    }
    
    static var fcbayern: Theme {
        let topIcon = UIImage(named: "fcbayern") ?? UIImage()
        let screenshot = UIImage(named: "fcbayern_screenshot") ?? UIImage()
        let mainColor = UIColor(red: 197/255.0, green: 31/255.0, blue: 48/255.0, alpha: 1)
        let primaryButtonBackgroundColor = mainColor
        let secondaryButtonBackgroundColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1)
        let tertiaryButtonBackgroundColor = secondaryButtonBackgroundColor
        let primaryButtonTextColor = UIColor.white
        let accessType = AccessType.menuSettings
        return Theme(topIcon: topIcon,
                     screenshot: screenshot,
                     mainColor: mainColor,
                     primaryButtonBackgroundColor: primaryButtonBackgroundColor,
                     secondaryButtonBackgroundColor: secondaryButtonBackgroundColor,
                     tertiaryButtonBackgroundColor: tertiaryButtonBackgroundColor,
                     primaryButtonTextColor: primaryButtonTextColor,
                     accessType: accessType)
    }

    
    enum Font: String {
        case light = "Avenir-Roman"
        case regular = "Avenir-Medium"
        case semibold = "Avenir-Heavy"
        case bold = "Avenir-Black"

        static func printAll() {
            let fontFamilyNames = UIFont.familyNames
            for familyName in fontFamilyNames {
                print("------------------------------")
                print("Font Family Name: \(familyName)")
                let names = UIFont.fontNames(forFamilyName: familyName)
                print("Font Names: \(names)")
            }
        }
    }
}

extension UIColor {
    
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat((hex >> 16) & 0xff) / 255.0,
                  green: CGFloat((hex >> 8) & 0xff) / 255.0,
                  blue: CGFloat(hex & 0xff) / 255.0,
                  alpha: alpha)
    }

}

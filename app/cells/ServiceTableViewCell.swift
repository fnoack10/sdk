//
//  ServiceTableViewCell.swift
//  UserCentrics
//
//  Created by Franco Noack on 08.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import UIKit
import UsercentricsSDK

protocol ServiceTableViewCellDelegate {
    func didUpdateService(service: ServicePM, indexPath: IndexPath)
}

class ServiceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var toggle: UISwitch!
    @IBOutlet weak var infoButton: UIButton!
    
    var delegate: ServiceTableViewCellDelegate?
    var service: ServicePM?
    var indexPath: IndexPath?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        toggle.addTarget(self, action: #selector(handleToggle(_:)), for: .valueChanged)
    }
    
    func configure(with service: ServicePM, indexPath: IndexPath) {
        self.indexPath = indexPath
        self.service = service
        titleLabel.text = service.name
        toggle.isOn = service.isConsented
        toggle.isEnabled = !service.isEssencial
    }
    
    @objc func handleToggle(_ sender: UISwitch) {
        guard let delegate = delegate, var service = service, let indexPath = indexPath else { return }
        service.isConsented = sender.isOn
        delegate.didUpdateService(service: service, indexPath: indexPath)
    }
    
}



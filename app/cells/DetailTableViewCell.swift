//
//  DetailTableViewCell.swift
//  UserCentrics
//
//  Created by Franco Noack on 07.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import UIKit
import UsercentricsSDK

class DetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        descriptionLabel.font = UIFont.systemFont(ofSize: 14)
    }
    
    func configure(with detail: ServiceDetailPM) {
        titleLabel.text = detail.title
        descriptionLabel.text = detail.description
    }
    
}



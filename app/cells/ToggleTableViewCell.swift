//
//  ToggleTableViewCell.swift
//  UserCentrics
//
//  Created by Franco Noack on 06.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import UIKit
import UsercentricsSDK

protocol ToggleTableViewCellDelegate {
    func didSwitchToggleForService(isConsented: Bool, indexPath: IndexPath)
}

class ToggleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var toggle: UISwitch!
    @IBOutlet weak var discloseView: UIButton!
    
    var delegate: ToggleTableViewCellDelegate?
    var indexPath: IndexPath?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()

    }
    
    @objc func handleToggle(_ sender: UISwitch) {
        guard let delegate = delegate, let indexPath = indexPath else { return }
        delegate.didSwitchToggleForService(isConsented: sender.isOn, indexPath: indexPath)
    }
    
    private func setupUI() {
        titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        descriptionLabel.font = UIFont.systemFont(ofSize: 14)
        toggle.addTarget(self, action: #selector(handleToggle(_:)), for: .valueChanged)
    }
    
    func configure(with category: CategoryPM, indexPath: IndexPath) {
        self.indexPath = indexPath
        titleLabel.text = category.name
        descriptionLabel.text = category.name
        toggle.isOn = category.isConsented
        toggle.isEnabled = !category.isEssencial
    }
    
    func configure(with service: ServicePM, indexPath: IndexPath) {
        self.indexPath = indexPath
        titleLabel.text = service.name
        descriptionLabel.text = service.categoryName
        toggle.isOn = service.isConsented
        toggle.isEnabled = !service.isEssencial
        discloseView.isSelected = service.isSelected
    }
    
}



//
//  CategoryListPM.swift
//  UserCentrics
//
//  Created by Franco Noack on 08.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import Foundation
import UsercentricsSDK

struct CategoryPM {
    let name: String
    let description: String
    let slug: String
    let isEssencial: Bool
    let isHidden: Bool
    
    var isSelected: Bool = false
    var isConsented: Bool = false
    
    init(_ category: UsercentricsSDK.Category, services: [ServicePM]) {
        self.name = category.label
        self.description = category.description()
        self.slug = category.slug
        self.isEssencial = category.isEssential
        self.isHidden = category.isHidden
        
        self.isConsented = services.compactMap { $0.isConsented }.contains(true)
    }
    
}

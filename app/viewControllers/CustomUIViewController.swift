//
//  ViewController.swift
//  usercentric-ios-demo
//
//  Created by Franco Noack on 05.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import UIKit
import UsercentricsSDK

enum AccessType {
    case privacyBubble
    case menuSettings
    case mainButton
}

class CustomUIViewController: UIViewController {
    
    var theme: Theme?
    var userCentrics: Usercentrics = Usercentrics(settingsId: "gChmbFIdL", options: UserOptions(controllerId: nil, language: "en", version: nil, debugMode: false))

    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var privacyButton: UIButton!
    @IBOutlet weak var screenshotImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var debugView: UIStackView!
    @IBOutlet weak var debugTextView: UITextView!
    @IBOutlet weak var settingsIDLabel: UILabel!
    @IBOutlet weak var settingsIDTextField: UITextField!
    @IBOutlet weak var settingsIDButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        launchUserCentrics()
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?){
        if motion == .motionShake {
            debugView.isHidden = !debugView.isHidden
        }
    }
    
    private func setupUI() {
        settingsButton.isHidden = true
        privacyButton.isHidden = true
        mainButton.isHidden = true
        debugView.isHidden = true
        debugTextView.text = ""
        
        settingsIDLabel.text = "SettingsID"
        settingsIDLabel.font = UIFont.boldSystemFont(ofSize: 14)
        settingsIDTextField.text = "gChmbFIdL"
        

        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    private func updateUI() {
        guard let theme = self.theme else { return }
        screenshotImage.image = theme.screenshot
        
        mainButton.setTitle("Open CMP", for: .normal)
        mainButton.setTitleColor(theme.primaryButtonTextColor, for: .normal)
        mainButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        mainButton.backgroundColor = theme.mainColor
        mainButton.layer.cornerRadius = 8.0
        
        settingsIDButton.setTitle("Apply", for: .normal)
        settingsIDButton.setTitleColor(theme.primaryButtonTextColor, for: .normal)
        settingsIDButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        settingsIDButton.backgroundColor = theme.mainColor
        settingsIDButton.layer.cornerRadius = 8.0
        
        switch theme.accessType {
        case .privacyBubble:
            settingsButton.isHidden = true
            privacyButton.isHidden = false
            mainButton.isHidden = true
        case .menuSettings:
            settingsButton.isHidden = false
            privacyButton.isHidden = true
            mainButton.isHidden = true
        case .mainButton:
            settingsButton.isHidden = true
            privacyButton.isHidden = true
            mainButton.isHidden = false
        }
    }
    
    private func launchUserCentrics() {
        userCentrics.initialize(callback: { isConsentViewShown in
            self.updateUI()
            self.takeActionIfNeeded(isConsentViewShown)
        }) { error in
            print(error)
        }
    }
    
    private func takeActionIfNeeded(_ isConsentViewShown: Usercentrics.INITIAL_VIEW) {
        switch isConsentViewShown {
        case .firstLayer:
            print("Show first Layer")

            performSegue(withIdentifier: "consentSegue", sender: nil)
        case .none:
            print("No action needed")
            break
        default:
            print("Extra case - \(isConsentViewShown)")
            break
        }
        
        activityIndicator.stopAnimating()
        setupDebug()
        subscribe()
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                navigationController?.popViewController(animated: true)
            default:
                break
            }
        }
    }
    
    private func setupDebug() {
        guard let settings = userCentrics.getSettings(),
            let categories = userCentrics.getCategories() else { return }
        let services = userCentrics.getServices()
        let language = userCentrics.getLanguage()
        debugTextView.text = "\(settings)\n\n\(services)\n\n\(categories)\n\n\(language)"
    }
    
    private func subscribe() {
        userCentrics.subscribeEvent(eventName: "uc_consent_status") { event in
            print("Event Change \(event)")
        }
    }
    
    private func updateLanguage(to language: String) {
//        userCentrics.changeLanguage(language: language) { _ in
//            // Callback to be executed once the event is fired from the framework
//            print("Langugae CHHANGE")
//        }
    }
    
    // MARK: - Actions
    
    @IBAction func privacyAction(_ sender: Any) {
        performSegue(withIdentifier: "consentSegue", sender: nil)
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let theme = self.theme else { return }
        if segue.identifier == "consentSegue" {
            if let destinationVC = segue.destination as? ConsentViewController {
                destinationVC.userCentrics = userCentrics
                destinationVC.theme = theme
            }
        }
    }

}


//
//  MenuViewController.swift
//  usercentric-ios-demo
//
//  Created by Franco Noack on 05.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var usercentricsButton: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var zalandoButton: UIButton!
    @IBOutlet weak var commerzbankButton: UIButton!
    @IBOutlet weak var fcbayernButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

    private func setupUI() {
 
        usercentricsButton.setTitle("Demo Usercentrics SDK", for: .normal)
        usercentricsButton.setTitleColor(UIColor.black, for: .normal)
        usercentricsButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        usercentricsButton.layer.borderWidth = 1.5
        usercentricsButton.layer.borderColor = UIColor.black.cgColor
        usercentricsButton.layer.cornerRadius = 8.0
        
        orLabel.text = "or"
        orLabel.textColor = UIColor.black
        orLabel.font = UIFont.systemFont(ofSize: 14)
        
        zalandoButton.setTitle("Launch Zalando", for: .normal)
        zalandoButton.setTitleColor(UIColor.black, for: .normal)
        zalandoButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        zalandoButton.layer.borderWidth = 1.5
        zalandoButton.layer.borderColor = UIColor.black.cgColor
        zalandoButton.layer.cornerRadius = 8.0
        
        commerzbankButton.setTitle("Launch Commerzbank", for: .normal)
        commerzbankButton.setTitleColor(UIColor.black, for: .normal)
        commerzbankButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        commerzbankButton.layer.borderWidth = 1.5
        commerzbankButton.layer.borderColor = UIColor.black.cgColor
        commerzbankButton.layer.cornerRadius = 8.0
        
        fcbayernButton.setTitle("Launch FC Bayern", for: .normal)
        fcbayernButton.setTitleColor(UIColor.black, for: .normal)
        fcbayernButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        fcbayernButton.layer.borderWidth = 1.5
        fcbayernButton.layer.borderColor = UIColor.black.cgColor
        fcbayernButton.layer.cornerRadius = 8.0
    }
    
    @IBAction func launchUsercentrics(_ sender: Any) {
        performSegue(withIdentifier: "launchApp", sender: Theme.usercentrics)
    }
    
    @IBAction func launchZalando(_ sender: Any) {
        performSegue(withIdentifier: "launchApp", sender: Theme.zalando)
    }
    
    @IBAction func launchCommerzbank(_ sender: Any) {
        performSegue(withIdentifier: "launchApp", sender: Theme.commerzbank)
    }
    
    @IBAction func launchFCBayern(_ sender: Any) {
        performSegue(withIdentifier: "launchApp", sender: Theme.fcbayern)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let theme = sender as? Theme else { return }
          if segue.identifier == "launchApp" {
              if let destinationVC = segue.destination as? CustomUIViewController {
                  destinationVC.theme = theme
              }
          }
      }
}


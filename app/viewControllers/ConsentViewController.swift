//
//  ConsentViewController.swift
//  UserCentrics
//
//  Created by Franco Noack on 05.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import UIKit
import UsercentricsSDK

class ConsentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, SelectorDelegate, ToggleTableViewCellDelegate, ServiceTableViewCellDelegate {

    var userCentrics: Usercentrics?
    var theme: Theme?
    var presentationModel: UsercentricsPM = UsercentricsPM()

    private var maxHeaderHeight: CGFloat = 100.0
    private var minHeaderHeight: CGFloat = 0.0
    private var headerHeight: CGFloat = 0.0
        
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var textHeaderView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var selectorView: SelectorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var aceptAllButton: UIButton!
    @IBOutlet weak var denyAllButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var poweredByLabel: UILabel!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyTheme()
        updateUI()
    }
    
    private func applyTheme() {
        guard let theme = theme else { return }

        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        }
        
        selectorView.configure(with: theme)
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        titleLabel.textColor = theme.textColor
        descriptionLabel.font = UIFont.systemFont(ofSize: 14)
        descriptionLabel.textColor = theme.textColor
        languageLabel.font = UIFont.systemFont(ofSize: 14)
        languageLabel.textColor = theme.textColor
        languageLabel.layer.borderColor = UIColor.init(hex: 0xDEDEDE).cgColor
        languageLabel.layer.borderWidth = 1.0
        languageLabel.layer.cornerRadius = 4.0
        poweredByLabel.font = UIFont.systemFont(ofSize: 10)
        poweredByLabel.textColor = theme.textColor
        
        iconImageView.image = theme.topIcon
        
        aceptAllButton.setTitleColor(theme.primaryButtonTextColor, for: .normal)
        aceptAllButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        aceptAllButton.backgroundColor = theme.primaryButtonBackgroundColor
        aceptAllButton.layer.cornerRadius = 8.0
        
        denyAllButton.setTitleColor(theme.secondaryButtonTextColor, for: .normal)
        denyAllButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        denyAllButton.backgroundColor = theme.secondaryButtonBackgroundColor
        denyAllButton.layer.cornerRadius = 8.0
        
        saveButton.setTitleColor(theme.tertiaryButtonTextColor, for: .normal)
        saveButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        saveButton.backgroundColor = theme.tertiaryButtonBackgroundColor
        saveButton.layer.cornerRadius = 8.0
    }
    
    private func updateUI() {
        guard let userCentrics = userCentrics,
            let categories = userCentrics.getCategories(),
            let settings = userCentrics.getSettings() else { return }
        let services = userCentrics.getServices()
        
        presentationModel = UsercentricsPM(settings: settings, categories: categories, services: services)
        
        selectorView.selectorDelegate = self
        titleLabel.text = settings.ui.firstLayer.title
        descriptionLabel.text = settings.ui.firstLayer.component1().default_ // settings.ui.firstLayer.description
        languageLabel.text = settings.ui.language.selected.uppercased()
        aceptAllButton.setTitle(settings.ui.buttons.acceptAll.label, for: .normal)
        denyAllButton.setTitle(settings.ui.buttons.denyAll.label, for: .normal)
        saveButton.setTitle(settings.ui.buttons.save.label, for: .normal)
        
        poweredByLabel.isHidden = true
//        if let partner = settings.ui.poweredBy.partnerUrlLabel {
//            poweredByLabel.text = settings.ui.poweredBy.label + " " + partner
//            poweredByLabel.isHidden = false
//        } else {
//            poweredByLabel.isHidden = true
//        }
        
        self.view.layoutIfNeeded()
        let maxHeaderHeight: CGFloat = textHeaderView.frame.size.height
        headerViewHeightConstraint.constant = maxHeaderHeight
        self.maxHeaderHeight = maxHeaderHeight
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = UITableView.automaticDimension
        headerHeight = maxHeaderHeight + selectorView.frame.height + topView.frame.height
        tableView.contentInset = UIEdgeInsets(top: headerHeight, left: 0, bottom: 60, right: 0)
    }

    // MARK - TableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch selectorView.selection {
        case .category: return presentationModel.categories.list.count
        case .service: return presentationModel.services.list.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectorView.selection {
        case .category:
            let category = presentationModel.categories.list[section]
            let categoryServices = presentationModel.services.servicesForCategory(category.slug)
            return category.isSelected ? 1 + categoryServices.count : 1
        case .service:
            let service = presentationModel.services.list[section]
            return service.isSelected ? 1 + service.details.count : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectorView.selection {
        case .category:
            if indexPath.row == 0 {
                let cellIdentifier = "ToggleTableViewCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ToggleTableViewCell
                let category = presentationModel.categories.list[indexPath.section]
                cell.delegate = self
                cell.configure(with: category, indexPath: indexPath)
                return cell
            } else {
                let cellIdentifier = "ServiceTableViewCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ServiceTableViewCell
                let category = presentationModel.categories.list[indexPath.section]
                let categoryServices = presentationModel.services.servicesForCategory(category.slug)
                let serviceIndex = indexPath.row - 1
                let service = categoryServices[serviceIndex]
                cell.delegate = self
                cell.configure(with: service, indexPath: indexPath)
                return cell
            }
        case .service:
            if indexPath.row == 0 {
                let cellIdentifier = "ToggleTableViewCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ToggleTableViewCell
                let service = presentationModel.services.list[indexPath.section]
                cell.delegate = self
                cell.configure(with: service, indexPath: indexPath)
                return cell
            } else {
                let cellIdentifier = "DetailTableViewCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DetailTableViewCell
                let detail = presentationModel.services.list[indexPath.section].details[indexPath.row - 1]
                cell.configure(with: detail)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch selectorView.selection {
        case .category:
            let isSelected = presentationModel.categories.list[indexPath.section].isSelected
            presentationModel.categories.list[indexPath.section].isSelected = !isSelected
        case .service:
            let isSelected = presentationModel.services.list[indexPath.section].isSelected
            presentationModel.services.list[indexPath.section].isSelected = !isSelected
        }
        tableView.reloadSections([indexPath.section], with: .automatic)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset
        let rawOffset = self.headerHeight + contentOffset.y
        let offset = CGFloat(round(1000*rawOffset)/1000)

        
        if offset < 0 { // Scroll Down
            self.headerViewHeightConstraint.constant = self.maxHeaderHeight
        } else if offset > 0 && self.headerViewHeightConstraint.constant >= self.minHeaderHeight { // Scroll Up
            self.headerViewHeightConstraint.constant = self.maxHeaderHeight-offset
            if self.headerViewHeightConstraint.constant < self.minHeaderHeight {
                self.headerViewHeightConstraint.constant = self.minHeaderHeight
            }
        }
 
        let alpha = self.headerViewHeightConstraint.constant / (self.maxHeaderHeight - self.minHeaderHeight)
        self.textHeaderView.alpha = alpha
    }
    
    // MARK - Selector Delegate
    
    func didSelect(_ selection: SelectionOptions) {
        tableView.reloadData()
    }
    
    // MARK - ToggleTableViewCellDelegate
    
    func didSwitchToggleForService(isConsented: Bool, indexPath: IndexPath) {
        switch selectorView.selection {
        case .category:
            presentationModel.categories.list[indexPath.section].isConsented = isConsented
            let category = presentationModel.categories.list[indexPath.section]
            presentationModel.services.setConsentForCategory(isConsented, categorySlug: category.slug)
            tableView.reloadSections([indexPath.section], with: .automatic)
        case .service:
            presentationModel.services.list[indexPath.section].isConsented = isConsented
        }
    }
    
    // MARK: - ServiceTableViewCellDelegate
    
    func didUpdateService(service: ServicePM, indexPath: IndexPath) {
        var updatedServices: [ServicePM] = []
        for i in presentationModel.services.list {
            if i.id == service.id {
                updatedServices.append(service)
            } else {
                updatedServices.append(i)
            }
        }
        presentationModel.services.list = updatedServices
        let isCategoryConsented = presentationModel.services.servicesForCategory(service.categorySlug).compactMap{ $0.isConsented}.contains(true)
        presentationModel.categories.list[indexPath.section].isConsented = isCategoryConsented
        tableView.reloadSections([indexPath.section], with: .automatic)
    }
    
    // MARK - Actions
    
    @IBAction func didTapLanguages(_ sender: UIButton) {
        guard let userCentrics = self.userCentrics,
            let settings = userCentrics.getSettings() else { return }
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for language in settings.ui.language.available {
            let action: UIAlertAction = UIAlertAction(title: language.uppercased() , style: .default) { action -> Void in
                userCentrics.changeLanguage(language: language, callback: {
                    self.updateUI()
                }, onFailure: { error in
                    print(error)
                })
            }
            actionSheetController.addAction(action)
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        actionSheetController.addAction(cancelAction)

        actionSheetController.popoverPresentationController?.sourceView = sender
        present(actionSheetController, animated: true)
    }
    
    private func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func aceptAllAction(_ sender: Any) {
        guard let userCentrics = userCentrics else { return }
        userCentrics.acceptAllServices(consentType: CONSENT_TYPE.explicit_, callback: {
            self.dismiss()
        }) { error in
            print(error)
        }
    }
    
    @IBAction func denyAllAction(_ sender: Any) {
        guard let userCentrics = userCentrics else { return }
        userCentrics.denyAllServices(consentType: CONSENT_TYPE.explicit_, callback: {
            self.dismiss()
        }) { error in
            print(error)
        }
    }
    @IBAction func saveAction(_ sender: Any) {
        guard let userCentrics = userCentrics else { return }
        
        var decisions = [UserDecision]()
        
        for service in presentationModel.services.list {
            decisions.append(UserDecision(serviceId: service.id, status: service.isConsented))
        }
    
        userCentrics.updateServices(decisions: decisions, consentType: CONSENT_TYPE.explicit_, callback: {
            self.dismiss()
        }) { error in
            print(error)
        }
    }
    
}


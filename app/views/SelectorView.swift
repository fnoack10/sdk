import UIKit

protocol SelectorDelegate {
    func didSelect(_ selection: SelectionOptions)
}

enum SelectionOptions {
    case category
    case service
}

final class SelectorView: UIView {

    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var serviceButton: UIButton!
    
    var selectorDelegate: SelectorDelegate?
    var selection: SelectionOptions = .category {
        didSet {
            selectorDelegate?.didSelect(selection)
        }
    }
    
    private var selectionMarkerView: UIView = UIView()
    private var sections: [String] = []
    private var tabs: [UIButton] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with theme: Theme) {
        categoryButton.setTitle("Categories", for: .normal)
        categoryButton.setTitleColor(theme.textColor, for: .normal)
        categoryButton.setTitleColor(theme.mainColor, for: .selected)
        
        serviceButton.setTitle("Services", for: .normal)
        serviceButton.setTitleColor(theme.textColor, for: .normal)
        serviceButton.setTitleColor(theme.mainColor, for: .selected)
        
        addSelectionMarkerView(with: theme)
        updateSelectedButton(with: selection)
    }
    
    private func addSelectionMarkerView(with theme: Theme) {
        layoutIfNeeded()
        selectionMarkerView.frame = CGRect(x: 0, y: 0, width: self.frame.width/2, height: 2)
        selectionMarkerView.backgroundColor = theme.mainColor
        self.addSubview(selectionMarkerView)
    }
    
    
    private func updateSelectedButton(with selection: SelectionOptions) {
        var x: CGFloat = 0.0
        switch selection {
        case .category:
            categoryButton.isSelected = true
            categoryButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            serviceButton.isSelected = false
            serviceButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            x = categoryButton.center.x
        case .service:
            categoryButton.isSelected = false
            categoryButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            serviceButton.isSelected = true
            serviceButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            x = serviceButton.center.x
        }
        
        UIView.animateKeyframes(withDuration: 0.25, delay: 0, options: .calculationModeCubicPaced, animations: {
            self.selectionMarkerView.center = CGPoint(x: x, y: self.frame.size.height-1)
        }, completion: nil)
        
        if selection != self.selection {
            self.selection = selection
        }
    }
    
    @IBAction func categorySelectedAction(_ sender: Any) {
        updateSelectedButton(with: .category)
    }
    
    @IBAction func serviceSelectedAction(_ sender: Any) {
        updateSelectedButton(with: .service)
    }
    
}

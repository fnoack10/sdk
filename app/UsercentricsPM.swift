//
//  UsercentricsPM.swift
//  UserCentrics
//
//  Created by Franco Noack on 08.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import Foundation
import UsercentricsSDK

struct UsercentricsPM {
    var settings: SettingsPM
    var categories: CategoryListPM
    var services: ServiceListPM
    
    init() {
        self.settings = SettingsPM()
        self.categories = CategoryListPM([], services: ServiceListPM([]))
        self.services = ServiceListPM([])
    }
    
    init(settings: Settings, categories: [UsercentricsSDK.Category], services: [Service]) {
        self.settings = SettingsPM(settings)
        self.services = ServiceListPM(services, categories: categories, settings: settings)
        self.categories = CategoryListPM(categories, services: self.services)
    }
    
}


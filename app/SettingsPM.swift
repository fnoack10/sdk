//
//  SettingsPM.swift
//  UserCentrics
//
//  Created by Franco Noack on 08.05.20.
//  Copyright © 2020 UserCentrics. All rights reserved.
//

import Foundation
import UsercentricsSDK

struct SettingsPM {
    var id: String = ""
    var controllerID: String = ""
    var version: String = ""
    
    init() {}
    
    init(_ settings: Settings) {
        self.id = settings.id
        self.controllerID = settings.controllerId
        self.version = settings.version
    }
}
